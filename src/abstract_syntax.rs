use rosary::{format::TreeFormat, vec_tree::Tree};

use crate::{
    abstract_syntax::{expr::Expr, pattern::Pattern},
    concrete_syntax::{
        error_tree,
        statements::{binding_pattern::BindingPattern, InnerStmt, Stmt as ConcreteStmt},
        symbols::Symbol,
        ConcreteSyntax,
    },
    result::Result,
};

pub mod expr;
pub mod literal;
pub mod module;
pub mod op;
pub mod pattern;
pub mod types;

/// A piece of abstract syntax has corresponding chunks of concrete syntax that
/// it can be extracted from.
pub trait AbstractSyntax<ConcreteSource: ConcreteSyntax>: Sized {
    fn from_concrete(source: ConcreteSource) -> Result<Self>;

    #[cfg(test)]
    fn test_parse(input: &str) -> Self {
        let concrete = ConcreteSource::test_parse(input);

        match Self::from_concrete(concrete) {
            Ok(val) => val,
            Err(e) => panic!("Got error parsing string as abstract syntax: {}", e),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Stmt<Id> {
    Let(Pattern<Id>, Option<Expr<Id>>, Expr<Id>),
    Arg(Pattern<Id>, Option<Expr<Id>>, Expr<Id>),
    Expr(Expr<Id>),
}

impl<Id: ToString> TreeFormat for Stmt<Id> {
    fn treeify(&self) -> Tree<String> {
        match self {
            Stmt::Let(pattern, maybe_type, val) => error_tree(
                "Let Statement",
                vec![
                    Some(pattern.treeify().map_head(&|h| format!("Pattern: {h}"))),
                    maybe_type
                        .as_ref()
                        .map(|t| t.treeify().map_head(&|h| format!("Type Annotation: {h}"))),
                    Some(val.treeify().map_head(&|h| format!("Value: {h}"))),
                ]
                .into_iter()
                .filter_map(|i| i)
                .collect(),
            ),
            Stmt::Arg(pattern, maybe_type, val) => error_tree(
                "Arg Statement",
                vec![
                    Some(pattern.treeify().map_head(&|h| format!("Pattern: {h}"))),
                    maybe_type
                        .as_ref()
                        .map(|t| t.treeify().map_head(&|h| format!("Type Annotation: {h}"))),
                    Some(val.treeify().map_head(&|h| format!("Value: {h}"))),
                ]
                .into_iter()
                .filter_map(|i| i)
                .collect(),
            ),
            Stmt::Expr(expr) => expr.treeify().map_head(&|h| format!("Statement: {h}")),
        }
    }
}

impl AbstractSyntax<ConcreteStmt> for Stmt<Symbol> {
    fn from_concrete(source: ConcreteStmt) -> Result<Self> {
        match source.to_inner() {
            InnerStmt::LetStmt(BindingPattern {
                pattern,
                type_annotation,
                value,
            }) => Ok(Self::Let(
                Pattern::from_concrete(pattern)?,
                type_annotation
                    .map(|e| Expr::from_concrete(e))
                    .transpose()?,
                Expr::from_concrete(value)?,
            )),
            InnerStmt::ArgStmt(BindingPattern {
                pattern,
                type_annotation,
                value,
            }) => Ok(Self::Arg(
                Pattern::from_concrete(pattern)?,
                type_annotation
                    .map(|e| Expr::from_concrete(e))
                    .transpose()?,
                Expr::from_concrete(value)?,
            )),
            InnerStmt::ExprSemicolon(e) => Expr::from_concrete(e).map(Self::Expr),
        }
    }
}

#[cfg(test)]
mod test {
    use num::{BigInt, BigRational};
    use rosary::format::TreeFormat;

    use crate::{
        abstract_syntax::{AbstractSyntax, Expr, Pattern},
        concrete_syntax::pattern::Pattern as ConcretePattern,
    };

    use super::literal::Literal;

    #[test]
    fn number() {
        let got = Expr::test_parse("3");
        let want = Expr::Literal(Literal::Num(BigRational::new(
            BigInt::from(3),
            BigInt::from(1),
        )));

        assert_eq!(got, want)
    }

    #[test]
    pub fn test_lambda_expr() {
        let got = Expr::test_parse("fn a => a");
        let want = Expr::Lambda {
            body: Box::new(Expr::test_parse("a")),
            params: vec![<Pattern<_> as AbstractSyntax<ConcretePattern>>::test_parse(
                "a",
            )],
        };

        assert_eq!(
            got,
            want,
            "\nGot {}\n=====\nWant {}",
            got.treeify(),
            want.treeify()
        )
    }

    #[test]
    pub fn test_applit_expr() {
        let got = Expr::test_parse("fn (Foo 1) Bar => Baz");
        let want = Expr::Lambda {
            params: vec![Pattern::test_parse("(Foo 1)"), Pattern::test_parse("Bar")],
            body: Box::new(Expr::test_parse("Baz")),
        };

        assert_eq!(
            got,
            want,
            "\nGot {}\n=====\nWant {}",
            got.treeify(),
            want.treeify()
        )
    }

    #[test]
    pub fn test_2apps_expr() {
        let got = Expr::test_parse("fn (Foo 1) (Bar 1) => Baz");
        let want = Expr::Lambda {
            params: vec![
                Pattern::test_parse("(Foo 1)"),
                Pattern::test_parse("(Bar 1)"),
            ],
            body: Box::new(Expr::test_parse("Baz")),
        };

        assert_eq!(
            got,
            want,
            "\nGot {}\n=====\nWant {}",
            got.treeify(),
            want.treeify()
        )
    }
}
