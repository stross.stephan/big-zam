use itertools::Itertools;
use rosary::vec_tree::Tree;

use crate::concrete_syntax::error_tree;

pub type Result<T> = std::result::Result<T, Tree<String>>;

/// Handler for the kind of results that are common in this parser. Given an
/// iterator of results where we expect a single success (parsing a disjunction,
/// for instance) and a way to convert extraneous successes to errors, we can
/// give a detailed report on why one of those errors occurred.
pub fn single_unique_result<'a, T: PartialEq>(
    ctx: impl std::fmt::Display,
    results: impl Iterator<Item = Result<T>>,
    render: impl Fn(T) -> Tree<String>,
) -> Result<T> {
    let (mut parses, errs): (Vec<_>, Vec<_>) = results.into_iter().partition_result();

    // We need to ensure we got a unique parse from the set of operators we
    // tried to parse.
    match parses.len() {
        0 => Err(error_tree(format!("{ctx}: No successes."), errs)),
        1 => Ok(parses.pop().expect("Pop failed after length check?")),
        _ => {
            // If they're all the same result, it shouldn't matter how we parsed
            // it.
            if parses.iter().all_equal() {
                Ok(parses.pop().expect("Pop failed after length check?"))
            } else {
                Err(error_tree(
                    format!("{ctx}: Conflicting successes"),
                    vec![
                        error_tree("Conflicts", parses.into_iter().map(render).collect()),
                        error_tree("Failures", errs),
                    ],
                ))
            }
        }
    }
}

pub fn list<'a, T>(
    ctx: impl std::fmt::Display,
    results: impl Iterator<Item = Result<T>>,
    render: impl Fn(T) -> Tree<String>,
) -> Result<Vec<T>> {
    let mut successes = Vec::new();
    let mut errors = Vec::with_capacity(0); // Optimistically don't alloc.

    let r = &render;

    for result in results {
        if !errors.is_empty() {
            // If we've already seen errors, that means they're all errors.
            errors.push(result.map_or_else(|e| e, r));
        } else {
            match result {
                Ok(val) => successes.push(val),
                Err(e) => {
                    // We've gotten our first error, drain all the prior
                    // successes into the error accumulator and add this new
                    // failure to then end.
                    errors = successes.into_iter().map(r).collect();
                    errors.push(e);
                    successes = Vec::with_capacity(0);
                }
            }
        }
    }

    if errors.is_empty() {
        Ok(successes)
    } else {
        Err(error_tree(ctx, errors))
    }
}
