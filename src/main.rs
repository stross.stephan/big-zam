#![feature(try_trait_v2)]

extern crate bimap;
extern crate pest;
#[macro_use]
extern crate pest_derive;
extern crate itertools;
extern crate lazy_static;
extern crate num_traits;
extern crate rosary;
extern crate tracing;
extern crate tracing_subscriber;

mod abstract_syntax;
mod concrete_syntax;
mod result;
mod utils;

use rosary::format::TreeFormat;
use tracing::Level;

use crate::abstract_syntax::module::ZamFile as AbstractModule;
use crate::{abstract_syntax::AbstractSyntax, concrete_syntax::module::ZamFile};

fn init_logging() {
    tracing::subscriber::set_global_default(
        tracing_subscriber::FmtSubscriber::builder()
            .with_max_level(Level::TRACE)
            .pretty()
            .finish(),
    )
    .expect("Failed to set up logger")
}

fn main() -> () {
    init_logging();
    let (name, concrete_module) = match ZamFile::from_path("zam-mod.zam") {
        Ok(res) => res,
        Err(errors) => panic!("Error tree: {}", errors.fmt_tree()),
    };

    // println!(
    //     "{}",
    //     concrete_module
    //         .treeify()
    //         .map_head(&|h| format!("Concrete {name}: {h}"))
    // );

    //   let mut interner = Interner::new();
    //   let mut intern_fn = |sym| interner.intern(sym);
    //   let module = module.map(&intern_fn);

    let abstract_module = match AbstractModule::from_concrete(concrete_module) {
        Ok(v) => v,
        Err(e) => panic!("Abstract parse error: {}", e),
    };
    println!(
        "{}",
        abstract_module
            .treeify()
            .map_head(&|h| format!("Abstract {name}: {h}"))
    );
}
