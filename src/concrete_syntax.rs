pub mod branches;
pub mod chars;
pub mod comments;
pub mod errors;
pub mod expr;
pub mod lambda;
pub mod literals;
pub mod module;
pub mod numbers;
pub mod pattern;
pub mod sequences;
pub mod statements;
pub mod strings;
pub mod symbols;

use std::fmt::Debug;

use pest::iterators::{Pair, Pairs};
use rosary::vec_tree::Tree;

use crate::result::Result;

#[derive(Parser)]
#[grammar = "zam.pest"]
pub struct ZamParser;

#[derive(Clone, Copy)]
pub struct FileLoc {
    pub line: usize,
    pub col: usize,
}

#[derive(Clone, Copy)]
pub struct FileSpan {
    pub start: FileLoc,
    pub end: FileLoc,
}

/// This trait should be implemented by any concrete syntax element that has a
/// direct representation in a file.
trait Positioned {
    /// Return the span covered by this syntax element, from the position of the
    /// first character(inclusive), to the last character (exclusive).
    fn span(&self) -> FileSpan;
}

/// A type representing a piece of concrete syntax, which can be parsed from a
/// single Pair and correspond to a single rule in the grammar.
pub trait ConcreteSyntax: Sized {
    const EXPECTED_RULE: Rule;

    /// The inner implementation of the parsing function, should not be invoked
    /// by anything but this trait.
    fn parse_impl(pair: Pair<Rule>) -> Result<Self>;

    /// Check that the given pair is of the expected rule before passing it on
    /// to the actual parsing
    fn parse_pair(pair: Pair<Rule>) -> Result<Self> {
        let rule = pair.as_rule();
        if rule != Self::EXPECTED_RULE {
            return Err(error_tree(
                format!("Expected Rule::{:?}, Got", Self::EXPECTED_RULE),
                vec![fmt_pair(&pair)],
            ));
        }

        Self::parse_impl(pair)
    }

    /// Parse the next pair in the iterator as this piece of concrete syntax.
    fn parse_next(missing_ctx: &str, pairs: &mut Pairs<Rule>) -> Result<Self> {
        let pair = pairs.next().ok_or_else(|| error(missing_ctx))?;
        Self::parse_pair(pair)
    }

    /// If there is another pair, parse it as this piece of concrete syntax, but
    /// if there isn't, return Ok(None). This only fails if the rule is of the
    /// wrong type.
    fn maybe_parse_next(pairs: &mut Pairs<Rule>) -> Result<Option<Self>> {
        let next_pair = if let Some(next) = pairs.next() {
            next
        } else {
            return Ok(None);
        };
        Self::parse_pair(next_pair).map(Some)
    }

    fn parse_next_if_matches(pairs: &mut Pairs<Rule>) -> Result<Option<Self>> {
        let next_matches = pairs
            .peek()
            .map(|p| p.as_rule() == Self::EXPECTED_RULE)
            .unwrap_or_default();

        if next_matches {
            Self::parse_next("parse next internal failure", pairs).map(Some)
        } else {
            Ok(None)
        }
    }

    fn parse_many(pairs: &mut Pairs<Rule>) -> Result<Vec<Self>> {
        let mut acc = Vec::new();
        while let Some(next) = Self::parse_next_if_matches(pairs)? {
            acc.push(next)
        }
        Ok(acc)
    }

    fn parse_str(input: &str) -> Result<Self> {
        use pest::Parser;

        let mut pairs = ZamParser::parse(Self::EXPECTED_RULE, input).map_err(|e| e.to_string())?;
        let pair = pairs
            .next()
            .ok_or_else(|| format!("No pairs in successful parse of {:?}", Self::EXPECTED_RULE))?;
        Self::parse_pair(pair)
    }

    /// This function works as parse_str, but automatically unwraps the result.
    #[cfg(test)]
    fn test_parse(test_str: &str) -> Self {
        use pest::Parser;

        let pair = ZamParser::parse(Self::EXPECTED_RULE, test_str)
            .unwrap_or_else(|e| {
                panic!("Failed to parse test string\n```\n{}\n```\ndue to {}", test_str, e)
            })
            .next()
            .unwrap_or_else(|| panic!("Successful parse did not produce a pair for rule {:?} with input string ```\n{test_str}\n```", Self::EXPECTED_RULE));

        Self::parse_pair(pair).expect("Failed when invoking parse_pair in test")
    }
}

/// Assert the rule type of the next pair and drop it.
pub fn drop_next(pairs: &mut Pairs<Rule>, rule: Rule) -> Result<()> {
    let actual_rule = pairs
        .next()
        .ok_or_else(|| format!("{:?} missing", rule))?
        .as_rule();

    if rule == actual_rule {
        Ok(())
    } else {
        Err(error(format!(
            "Rule {:?} did not match {:?}",
            actual_rule, rule
        )))
    }
}

pub fn drop_next_back(pairs: &mut Pairs<Rule>, rule: Rule) -> Result<()> {
    let actual_rule = pairs
        .next_back()
        .ok_or_else(|| format!("{:?} missing", rule))?
        .as_rule();

    if rule == actual_rule {
        Ok(())
    } else {
        Err(error(format!(
            "Rule {:?} did not match {:?}",
            actual_rule, rule
        )))
    }
}

/// If there is another pair, assert the rule type. Returns whether or not there
///  was a pair.
pub fn maybe_drop_next(pairs: &mut Pairs<Rule>, rule: Rule) -> Result<bool> {
    let next = pairs.next();
    if let Some(next) = next {
        if next.as_rule() == rule {
            Ok(true)
        } else {
            Err(format!("Expected rule separator but got {:?}", next.as_rule()).into())
        }
    } else {
        Ok(false)
    }
}

/// convert a list of errors into a formattable tree.
pub fn error_tree<S: ToString, R: Into<Tree<String>>>(ctx: S, errs: Vec<R>) -> Tree<String> {
    Tree::Branch(ctx.to_string(), errs.into_iter().map(R::into).collect())
}

pub fn error<S: ToString>(msg: S) -> Tree<String> {
    Tree::Leaf(msg.to_string())
}

pub fn fmt_pair(pair: &Pair<Rule>) -> Tree<String> {
    let rule = pair.as_rule();
    let span = pair.as_str();
    let child_rules = pair
        .clone()
        .into_inner()
        .map(|p| fmt_pair(&p))
        .collect::<Vec<_>>();

    if child_rules.len() > 0 {
        let rule_info = format!("{:?}", rule);
        Tree::Branch(rule_info, child_rules)
    } else {
        let rule_info = format!("{:?}({})", rule, span.trim());
        Tree::Leaf(rule_info)
    }
}

pub fn print_pair(ctx: &str, pair: &Pair<Rule>) {
    let formatted = fmt_pair(pair);
    println!("{ctx}: {formatted}");
}

pub fn print_pairs(ctx: &str, pairs: &Pairs<Rule>) {
    let tree = Tree::Branch(
        ctx.to_string(),
        pairs
            .clone()
            .into_iter()
            .map(|pair| fmt_pair(&pair))
            .collect(),
    );
    println!("{tree}");
}

/// Useful when there's exactly one number of child pairs a pair might have, and
/// you want to directly extract them.
pub fn take_exactly<const NUM_ELEMS: usize>(pair: Pair<Rule>) -> Result<[Pair<Rule>; NUM_ELEMS]> {
    use std::mem::MaybeUninit;

    let mut pairs = pair.into_inner();

    unsafe {
        let mut result: [MaybeUninit<Pair<Rule>>; NUM_ELEMS] = MaybeUninit::uninit().assume_init();

        let mut elems_so_far = 0;
        for slot in &mut result[..] {
            match pairs.next() {
                Some(item) => {
                    slot.write(item);
                    elems_so_far += 1;
                }
                None => {
                    let error = error_tree(
                        format!(
                            "Provided pairs did not have at least {} elements. Got:",
                            NUM_ELEMS
                        ),
                        (0..elems_so_far)
                            .map(|index| fmt_pair(&result[index].assume_init_read()))
                            .collect(),
                    );

                    for mut to_drop in &mut result[0..elems_so_far] {
                        MaybeUninit::assume_init_drop(&mut to_drop)
                    }
                    return Err(error.into());
                }
            };
        }

        let remaining: Vec<_> = pairs.collect();
        if remaining.len() != 0 {
            // Something weird about the MaybeUninit type and closures is making
            // this necessary.
            let mut acc = Vec::with_capacity(result.len());
            for r in result {
                let init = r.assume_init();
                acc.push(fmt_pair(&init))
            }

            let error = error_tree(
                "Provided pairs had extra elements.",
                vec![
                    error_tree("Expected Values", acc),
                    error_tree(
                        "Extra Values",
                        remaining.into_iter().map(|r| fmt_pair(&r)).collect(),
                    ),
                ],
            );

            return Err(error);
        }

        Ok(result.map(|r| r.assume_init()))
    }
}
