use itertools::{Either, Itertools};
use num::{BigRational, Num};
use rosary::{format::TreeFormat, vec_tree::Tree};

use crate::{
    abstract_syntax::{AbstractSyntax, Expr},
    concrete_syntax::{
        self,
        chars::Character,
        error_tree,
        literals::Literal as ConcreteLiteral,
        numbers::{NumSeq, Number, Sign},
        symbols::{QualName, Symbol},
    },
    result::Result,
};

#[derive(Debug, Clone, PartialEq)]
pub enum Literal<Id> {
    Unit,
    Bool(bool),
    Num(BigRational),
    Str(Vec<Either<String, Expr<Id>>>),
    Char(Character),
    Name(Id),
    ScopeRef(QualName),
}

impl<Id> Literal<Id> {
    pub fn as_id_ref(&self) -> Option<&Id> {
        if let Self::Name(n) = self {
            Some(n)
        } else {
            None
        }
    }
}

impl AbstractSyntax<ConcreteLiteral> for Literal<Symbol> {
    fn from_concrete(source: ConcreteLiteral) -> Result<Self> {
        match source {
            ConcreteLiteral::Num(num_lit) => {
                let Number { base, sign, digits } = num_lit;
                let radix = base.unwrap_or_default().radix();
                let sign = sign.unwrap_or_default();

                let (significand, denom_zeroes) = match digits {
                    NumSeq::WholeOnly { text, .. } => (text, 0),
                    NumSeq::BothParts { mut whole, frac } => {
                        let zeroes = frac.len();
                        whole.push_str(&frac);
                        (whole, zeroes)
                    }
                    NumSeq::FracOnly(frac) => {
                        let zeroes = frac.len();
                        (frac, zeroes)
                    }
                };
                let rational_fmt = format!("{}/1{}", significand, "0".repeat(denom_zeroes));
                BigRational::from_str_radix(&rational_fmt, radix)
                    .map(|r| match sign {
                        Sign::Positive => r,
                        Sign::Negative => -r,
                    })
                    .map(Self::Num)
                    .map_err(|e| (panic!("Got error parsing number: {}", e)))
            }
            ConcreteLiteral::Str(str_lit) => {
                let (chunks, errs): (Vec<_>, Vec<_>) = str_lit
                    .into_iter()
                    .map(|chunk| match chunk {
                        concrete_syntax::strings::StringChunk::Plain(chunk) => Ok(Either::Left(
                            chunk.into_iter().map(|ch| *ch).collect::<String>(),
                        )),
                        concrete_syntax::strings::StringChunk::Interp(expr) => {
                            Expr::from_concrete(expr).map(Either::Right)
                        }
                    })
                    .partition_result();

                if errs.len() > 0 {
                    return Err(error_tree("Failures parsing interpolated exprs", errs));
                }

                Ok(Self::Str(chunks))
            }
            ConcreteLiteral::Sym(name) => Ok(name.try_as_symbol().either(
                |symbol| match symbol.as_ref() {
                    "true" => Literal::Bool(true),
                    "false" => Literal::Bool(false),
                    _ => Literal::Name(symbol.into()),
                },
                |qual_name| Literal::ScopeRef(qual_name),
            )),
            ConcreteLiteral::Char(char_lit) => Ok(Literal::Char(char_lit.0)),
            ConcreteLiteral::Unit => Ok(Literal::Unit),
        }
    }
}

impl<Id: ToString> TreeFormat for Literal<Id> {
    fn treeify(&self) -> Tree<String> {
        match self {
            Literal::Unit => "()".to_string().into(),
            Literal::Bool(b) => b.to_string().into(),
            Literal::Num(n) => format!("{n}").into(),
            Literal::Str(s) => {
                let mut template = String::new();
                let mut interpolated = Vec::new();
                let mut expr_idx = 0;
                for part in s {
                    match part {
                        Either::Left(chunk) => template.push_str(chunk),
                        Either::Right(expr) => {
                            template.push_str(&format!("{{{expr_idx}}}"));
                            expr_idx += 1;
                            interpolated.push(expr);
                        }
                    }
                }

                if interpolated.len() == 0 {
                    template.into()
                } else {
                    let interp_trees = interpolated
                        .into_iter()
                        .enumerate()
                        .map(|(idx, expr)| expr.treeify().map_head(&|h| format!("{{{idx}}}: {h}")));

                    Tree::Branch(template, interp_trees.collect())
                }
            }
            Literal::Char(c) => format!("'{:?}'", c).into(),
            Literal::Name(name) => name.to_string().into(),
            Literal::ScopeRef(refd) => refd.to_string().into(),
        }
    }
}
