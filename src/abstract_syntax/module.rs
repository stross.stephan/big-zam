use std::{collections::HashMap, iter};

use itertools::Either;
use rosary::{format::TreeFormat, vec_tree::Tree};

use crate::{
    abstract_syntax::{AbstractSyntax, Stmt},
    concrete_syntax::{module::ZamFile as ConcreteFile, symbols::Symbol},
    result::{self, Result},
};

pub struct ZamFile<Id> {
    pub body: Vec<Stmt<Id>>,
    pub children: HashMap<Id, Self>,
}

impl AbstractSyntax<ConcreteFile> for ZamFile<Symbol> {
    fn from_concrete(ConcreteFile { body, children, .. }: ConcreteFile) -> Result<Self> {
        let body_result = result::list(
            "Parsing module body",
            body.into_iter()
                .filter_map(Either::left)
                .map(Stmt::from_concrete),
            |s| s.treeify(),
        );
        let submod_result = result::list(
            "Parsing module children",
            children
                .into_iter()
                .map(|(sym, module)| Self::from_concrete(module).map(|m| (sym, m))),
            |s| s.1.treeify().swap_head(s.0.to_string()).1,
        );

        match (body_result, submod_result) {
            (Ok(body), Ok(children)) => Ok(Self {
                body,
                children: children.into_iter().collect(),
            }),
            (Ok(body), Err(err)) => Err(Tree::Branch(
                "Child modules failed to parse".to_string(),
                body.into_iter()
                    .map(|b| b.treeify())
                    .chain(iter::once(err))
                    .collect(),
            )),
            (Err(err), Ok(children)) => Err(Tree::Branch(
                "Parent module failed to parse".to_string(),
                iter::once(err)
                    .chain(
                        children
                            .into_iter()
                            .map(|c| c.1.treeify().swap_head(c.0.to_string()).1),
                    )
                    .collect(),
            )),
            (Err(body_err), Err(children_errs)) => Err(Tree::Branch(
                "Both parent and children failed to parse".to_string(),
                vec![body_err, children_errs],
            )),
        }
    }
}

impl<Id: ToString> TreeFormat for ZamFile<Id> {
    fn treeify(&self) -> Tree<String> {
        let mut branches = Vec::with_capacity(2);

        {
            let body_stmts = self.body.iter().map(|s| s.treeify()).collect::<Vec<_>>();
            let body_arm = if body_stmts.len() > 0 {
                Tree::Branch("Body".to_string(), body_stmts)
            } else {
                Tree::Leaf("Empty Body".to_string())
            };
            branches.push(body_arm)
        };

        if self.children.len() > 0 {
            branches.push(Tree::Branch(
                "Child modules".to_string(),
                self.children
                    .iter()
                    .map(|(name, module)| module.treeify().swap_head(name.to_string()).1)
                    .collect(),
            ));
        }

        Tree::Branch("Module".to_string(), branches)
    }
}
