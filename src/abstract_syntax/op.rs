use std::{
    collections::{HashMap, HashSet, VecDeque},
    convert::TryFrom,
};

use itertools::{Either, Itertools};
use rosary::{format::TreeFormat, vec_tree::Tree};

use crate::{
    abstract_syntax::{pattern::Pattern, Expr as AbsExpr},
    concrete_syntax::{error, error_tree, symbols::Symbol, ConcreteSyntax},
    result::{self, single_unique_result, Result},
    utils::shrinkwrap::{ShrinkStep, ShrinkWrapper},
};

type Expr = AbsExpr<Symbol>;

/// See "Parsing Mixfix Operators" for the deets on this. "Nils Anders
/// Danielsson" and "Ulf Norell" are the names you're looking for.
/// This is the version of the PDF I found, but no promises it'll still be there
/// when you look at this.
/// https://www.cse.chalmers.se/~nad/publications/danielsson-norell-mixfix.pdf

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Assoc {
    Left,
    Right,
    Non,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Fixity {
    Prefix,
    Infix(Assoc),
    Postfix,
    Closed,
}

impl std::fmt::Display for Fixity {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            Fixity::Prefix => "Prefix",
            Fixity::Infix(assoc) => match assoc {
                Assoc::Left => "Left Assoc",
                Assoc::Right => "Right Assoc",
                Assoc::Non => "Non Assoc",
            },
            Fixity::Postfix => "Postfix",
            Fixity::Closed => "Closed",
        };

        write!(f, "{s}")
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct OpSpec {
    pub fixity: Fixity,
    pub name: Symbol,
    pub fragments: Vec<Symbol>,
}

impl OpSpec {
    /// Returns the number of holes which are fully internal to the fragment
    /// sequence.
    pub fn internal_arity(&self) -> usize {
        self.fragments.len() - 1
    }

    pub fn arity(&self) -> usize {
        let internal = self.internal_arity();
        match self.fixity {
            Fixity::Prefix => 1 + internal,
            Fixity::Infix(_) => 1 + internal + 1,
            Fixity::Postfix => internal + 1,
            Fixity::Closed => internal,
        }
    }

    pub fn as_expr(&self, mut args: Vec<Expr>) -> Result<Expr> {
        if self.name == Symbol::parse_str("lambda")? {
            let params = result::list(
                "Parsing lambda pattern params",
                match args.remove(0) {
                    // An "application" is what the argument list to a lambda def
                    // looks like.
                    Expr::Appl { head, mut args } => {
                        args.insert(0, *head);
                        args
                    }
                    e => vec![e],
                }
                .into_iter()
                .map(Pattern::try_from),
                |p| p.treeify(),
            )?;
            let body = Box::new(args.remove(0));

            Ok(Expr::Lambda { params, body })
        } else {
            Ok(Expr::Appl {
                head: Box::new(self.name.clone().into()),
                args,
            })
        }
    }

    pub fn name(&self) -> Symbol {
        self.name.clone()
    }
}

impl TreeFormat for OpSpec {
    fn treeify(&self) -> Tree<String> {
        let Self {
            fixity,
            name,
            fragments,
        } = self;

        error_tree(
            format!("Operator Spec `{name}` with fixity {fixity}"),
            vec![error_tree(
                "Fragments",
                fragments.iter().map(|s| format!("{s}")).collect(),
            )],
        )
    }
}

fn default_ops() -> Vec<(OpSpec, HashSet<Symbol>)> {
    fn from_parts<S: AsRef<str>>(fixity: Fixity, name: S, parts: Vec<S>) -> OpSpec {
        OpSpec {
            fixity,
            name: Symbol::parse_str(name.as_ref())
                .unwrap_or_else(|e| panic!("Failed to parse symbol for built-in operator {}", e)),
            fragments: parts
                .into_iter()
                .map(|p| {
                    Symbol::parse_str(p.as_ref()).unwrap_or_else(|e| {
                        panic!(
                            "Failed to parse the symbol `{}`'s fragment {} due to:\n{}",
                            name.as_ref(),
                            p.as_ref(),
                            e
                        )
                    })
                })
                .collect(),
        }
    }

    // Math operators
    let fac_op = (
        from_parts(Fixity::Postfix, "fac", vec!["!"]),
        HashSet::with_capacity(0),
    );

    let exp_op = (
        from_parts(Fixity::Infix(Assoc::Right), "exp", vec!["^"]),
        [fac_op.0.name()].into(),
    );

    let mul_op = (
        from_parts(Fixity::Infix(Assoc::Left), "mul", vec!["*"]),
        [fac_op.0.name(), exp_op.0.name()].into(),
    );

    let div_op = (
        from_parts(Fixity::Infix(Assoc::Left), "div", vec!["/"]),
        [fac_op.0.name(), exp_op.0.name()].into(),
    );

    let rem_op = (
        from_parts(Fixity::Infix(Assoc::Left), "rem", vec!["%"]),
        [fac_op.0.name.clone(), exp_op.0.name.clone()].into(),
    );

    let sub_op = (
        from_parts(Fixity::Infix(Assoc::Left), "sub", vec!["-"]),
        [
            fac_op.0.name.clone(),
            exp_op.0.name.clone(),
            mul_op.0.name.clone(),
            div_op.0.name.clone(),
            rem_op.0.name.clone(),
        ]
        .into(),
    );

    let add_op = (
        from_parts(Fixity::Infix(Assoc::Left), "add", vec!["+"]),
        [
            fac_op.0.name(),
            exp_op.0.name(),
            mul_op.0.name(),
            div_op.0.name(),
            rem_op.0.name(),
        ]
        .into(),
    );

    // Logical operators
    let not_op = (
        from_parts(Fixity::Prefix, "not", vec!["not"]),
        HashSet::with_capacity(0),
    );

    let and_op = (
        from_parts(Fixity::Infix(Assoc::Left), "and", vec!["and"]),
        [not_op.0.name()].into(),
    );

    let or_op = (
        from_parts(Fixity::Infix(Assoc::Left), "or", vec!["or"]),
        [not_op.0.name(), and_op.0.name()].into(),
    );

    // Equality ops are non-associative and the weakest binding.
    let eq_op = (
        from_parts(Fixity::Infix(Assoc::Non), "eq", vec!["=="]),
        [
            fac_op.0.name(),
            exp_op.0.name(),
            mul_op.0.name(),
            div_op.0.name(),
            rem_op.0.name(),
            sub_op.0.name(),
            add_op.0.name(),
            not_op.0.name(),
            and_op.0.name(),
            or_op.0.name(),
        ]
        .into(),
    );

    let neq_op = (
        from_parts(Fixity::Infix(Assoc::Non), "neq", vec!["!="]),
        [
            fac_op.0.name(),
            exp_op.0.name(),
            mul_op.0.name(),
            div_op.0.name(),
            rem_op.0.name(),
            sub_op.0.name(),
            add_op.0.name(),
            not_op.0.name(),
            and_op.0.name(),
            or_op.0.name(),
        ]
        .into(),
    );

    // Function operators
    let arrow_op = (
        from_parts(Fixity::Infix(Assoc::Right), "arrow", vec!["->"]),
        [
            fac_op.0.name(),
            exp_op.0.name(),
            mul_op.0.name(),
            div_op.0.name(),
            rem_op.0.name(),
            sub_op.0.name(),
            add_op.0.name(),
            eq_op.0.name(),
            neq_op.0.name(),
        ]
        .into(),
    );

    vec![
        add_op, mul_op, sub_op, div_op, exp_op, rem_op, fac_op, and_op, or_op, not_op, arrow_op,
        eq_op, neq_op,
    ]
}

/// Format one of several conflicting successful parses together (along with
/// their input tails).
fn parse_to_err((parsed, rest): (Expr, &[Expr])) -> Tree<String> {
    error_tree(
        "Abstract Expr parse success",
        vec![
            parsed.treeify().map_head(&|h| format!("Parsed: {h}")),
            error_tree("Remaining", rest.iter().map(|e| e.treeify()).collect()),
        ],
    )
}

/// Formats the result of parsing an operator to an error.
fn op_parse_to_err<'o, 'r>(
    (op, parsed, rest): (&'o OpSpec, Vec<Expr>, &'r [Expr]),
) -> Tree<String> {
    error_tree(
        "Operator parsing success",
        vec![
            format!("Parsing op {}", op.name.as_ref()).into(),
            error_tree("Parsed", parsed.into_iter().map(|e| e.treeify()).collect()),
            error_tree("Remaining", rest.into_iter().map(|e| e.treeify()).collect()),
        ],
    )
}

/// Match a fragment and advance the input stream. There's nothing
/// meaningful to get from an operator fragment, so it doesn't need to parse
/// anything else out.
fn match_fragment<'r>(fragment: &Symbol, juxt: &'r [Expr]) -> Result<&'r [Expr]> {
    let (keyword, juxt) = juxt.split_first().ok_or_else(|| {
        error_tree(
            format!("Failed reading fragment `{fragment}` from expr sequence"),
            juxt.into_iter().map(|e| e.treeify()).collect(),
        )
    })?;

    let keyword = keyword
        .as_id_ref()
        .ok_or_else(|| error_tree("Not a fragment", vec![keyword.treeify()]))?;

    if keyword == fragment {
        Ok(juxt)
    } else {
        Err(format!("Actual fragment `{keyword}` != expected fragment `{fragment}`").into())
    }
}

struct OpNode {
    /// Op name mappings contained at this precedence level
    pub ops: HashMap<Symbol, OpSpec>,
    /// Indexes into `op_nodes` for the parent precedence graph. Only the owning
    /// precedence graph should touch these.
    pub weaker: HashSet<usize>,
    /// Indexes into `op_nodes` for the parent precedence graph. Only the owning
    /// precedence graph should touch these.
    pub stronger: HashSet<usize>,
}

impl OpNode {
    pub fn try_merge(left: OpNode, right: OpNode) -> Either<OpNode, (OpNode, OpNode)> {
        if left.weaker == right.weaker && left.stronger == right.stronger {
            let OpNode {
                mut ops,
                weaker,
                stronger,
            } = left;
            right
                .ops
                .into_iter()
                .for_each(|(name, op)| if let Some( op) = ops.insert(name, op) {
                    panic!("There shouldn't be situations where two nodes exist with colliding operator sets {}", op.treeify())
                });

            Either::Left(Self {
                ops,
                weaker,
                stronger,
            })
        } else {
            Either::Right((left, right))
        }
    }

    pub fn rewrite(self, from: usize, to: usize) -> Self {
        let Self {
            ops,
            weaker,
            stronger,
        } = self;

        Self {
            ops,
            weaker: weaker
                .into_iter()
                .map(|idx| if idx == from { to } else { idx })
                .collect(),
            stronger: stronger
                .into_iter()
                .map(|idx| if idx == from { to } else { idx })
                .collect(),
        }
    }

    pub fn op_set_label(&self) -> String {
        format!("[{}]", self.ops.iter().map(|(name, _)| name).join(", "))
    }

    pub fn with_fixity(&self, fixity: Fixity) -> impl Iterator<Item = &OpSpec> {
        self.ops.values().filter(move |op| op.fixity == fixity)
    }
}

impl TreeFormat for OpNode {
    fn treeify(&self) -> Tree<String> {
        let OpNode {
            ops,
            weaker,
            stronger,
        } = self;

        error_tree(
            "Op Node",
            vec![
                error_tree(
                    "Operators",
                    ops.iter()
                        .sorted_by(|(l, _), (r, _)| l.cmp(r))
                        .map(|(_, op)| op.treeify())
                        .collect(),
                ),
                error(format!(
                    "Weaker indices: [{}]",
                    weaker.iter().sorted().join(", ")
                )),
                error(format!(
                    "Strong indices: [{}]",
                    stronger.iter().sorted().join(", ")
                )),
            ],
        )
    }
}

pub struct PrecedenceGraph {
    // Indices into this vector exist throughout it and its owned precedence
    // nodes.
    op_nodes: HashMap<usize, OpNode>,
    // What precedence node is each operator in?
    name_map: HashMap<Symbol, usize>,
    // Used to terminate function application parsing.
    all_fragments: HashSet<Symbol>,
}

impl PrecedenceGraph {
    pub fn try_new(op_strength_map: Vec<(OpSpec, HashSet<Symbol>)>) -> Result<Self> {
        // Container for the top-level precedence nodes.
        let op_nodes = HashMap::new();
        // Mapping of operator names to containing node index.
        let name_map = HashMap::new();
        // A mapping from node index to all the nodes weaker than it.
        let weaker_map = HashMap::new();
        let all_fragments = HashSet::new();

        let (mut op_nodes, name_map, weaker_map, all_fragments) = ShrinkWrapper::new(
            op_strength_map.into_iter(),
        )
        .shrink(
            (op_nodes, name_map, weaker_map, all_fragments),
            &mut |(op, stronger), (mut op_nodes, mut name_map, mut weaker_map, mut all_fragments)| {
                // Check that a node with this operator's name doesn't already
                // exist in the map.
                if name_map.contains_key(&op.name) {
                    return ShrinkStep::Bail(
                        (op, stronger),
                        (op_nodes, name_map, weaker_map, all_fragments),
                    );
                }
                // Check that all the stronger operators than this exist
                // in the node map already. If not, this node can't be added.
                if !stronger
                    .iter()
                    .all(|stronger_op| name_map.contains_key(stronger_op))
                {
                    return ShrinkStep::Pass(
                        (op, stronger),
                        (op_nodes, name_map, weaker_map, all_fragments),
                    );
                }

                // We're clear to add the node, get the new index that this node
                // is going to have once added. We still need ownership of `op`
                // for the time being, so we defer adding it till the end.
                let new_index = op_nodes.len();

                // Generate the set of stronger node indices
                let stronger: HashSet<_> = stronger
                    .iter()
                    .filter_map(|name| name_map.get(name).cloned())
                    .collect();

                // Populate the "weaker than" map with the new information.
                stronger.iter().for_each(|strong_idx| {
                    weaker_map
                        .entry(*strong_idx)
                        .or_insert(HashSet::new())
                        .insert(new_index);
                });

                op.fragments.iter().for_each(|fragment| {
                    all_fragments.insert(fragment.clone());
                });

                // Update name map with the new operator and the operator node.
                name_map.insert(op.name(), new_index);
                op_nodes.insert(
                    new_index,
                    OpNode {
                        ops: [(op.name(), op)].into(),
                        weaker: HashSet::new(),
                        stronger,
                    },
                );

                ShrinkStep::Take((op_nodes, name_map, weaker_map, all_fragments))
            },
        )
        .map_err(|((op_nodes, name_map, weaker_map, _), leftovers)| {
            error_tree(
                "Failed to shrink operator list by precedence",
                vec![
                    error_tree(
                        "Op nodes",
                        op_nodes
                            .into_iter()
                            .sorted_by(|(id1, _), (id2, _)| id1.cmp(id2))
                            .map(|(_, node)| node.treeify())
                            .collect(),
                    ),
                    error(format!(
                        "Name indices [{}]",
                        name_map
                            .into_iter()
                            .map(|(name, idx)| format!("{idx}:{name}"))
                            .join(", ")
                    )),
                    error_tree(
                        "Weaker map",
                        weaker_map
                            .into_iter()
                            .map(|(strong, weak)| {
                                format!(
                                    "{strong} -> [{}]",
                                    weak.into_iter().map(|w| w.to_string()).join(", ")
                                )
                            })
                            .collect(),
                    ),
                    error_tree(
                        "Unincluded ops",
                        leftovers
                            .into_iter()
                            .map(|(op, stronger)| {
                                format!(
                                    "{} -> [{}]",
                                    op.name(),
                                    stronger.into_iter().map(|s| s.to_string()).join(", ")
                                )
                            })
                            .collect(),
                    ),
                ],
            )
        })?;

        // Include weaker maps into the appropriate precedence nodes.
        weaker_map.into_iter().for_each(|(stronger, weaker)| {
            op_nodes
                .entry(stronger)
                .and_modify(|node| node.weaker = weaker);
        });

        Ok(Self {
            op_nodes,
            name_map,
            all_fragments,
        }
        .compact())
    }

    fn compact(self) -> Self {
        fn rewrite_nodes(
            nodes: VecDeque<(usize, OpNode)>,
            names: HashMap<Symbol, usize>,
            from: usize,
            to: usize,
        ) -> (VecDeque<(usize, OpNode)>, HashMap<Symbol, usize>) {
            (
                nodes
                    .into_iter()
                    .map(|(idx, n)| (idx, n.rewrite(from, to)))
                    .collect(),
                names
                    .into_iter()
                    .map(|(name, node_id)| (name, if node_id == from { to } else { node_id }))
                    .collect(),
            )
        }

        let Self {
            op_nodes,
            mut name_map,
            all_fragments,
        } = self;
        let mut op_nodes: VecDeque<_> = op_nodes.into_iter().collect();

        // We accumulate nodes as we check them (in the below ShrinkWrapper)
        // in here, and once we run out of values in op_nodes, we reassign
        // op_nodes to this value.
        let mut merged = VecDeque::new();
        let mut changed = true;
        while changed {
            // Reset the watch variable that lets us know if iteration is making
            // progress.
            changed = false;

            // This loop runs until we've drained every value from op_nodes.
            while let Some((curr_idx, curr)) = op_nodes.pop_front() {
                // We take the rest of the nodes, and try and shrink them down into
                // the given node.
                let (replaced, merge_node, unmerged) = ShrinkWrapper::new(op_nodes.into_iter())
                    .shrink(
                        (HashSet::new(), curr),
                        &mut |(right_idx, other), (mut replaced_ids, curr)| match OpNode::try_merge(
                            curr, other,
                        ) {
                            Either::Left(merged) => {
                                replaced_ids.insert(right_idx);
                                ShrinkStep::Take((replaced_ids, merged))
                            }
                            Either::Right((curr, right)) => {
                                ShrinkStep::Pass((right_idx, right), (replaced_ids, curr))
                            }
                        },
                    )
                    // This shouldn't be able to happen. It's expected that there
                    // will be multiple operators at the end of shrinking. We'll
                    // rewrite anyway, though, in the event a weirdly restricted
                    // operator set is being used.
                    .map(|(replaced, merged)| (replaced, merged, VecDeque::with_capacity(0)))
                    // The expected case. We anticipate having operators that we
                    // didn't merge into this one, but see above comment.
                    .unwrap_or_else(|((replaced, merged), unmerged)| (replaced, merged, unmerged));

                // If replaced isn't empty, that means we merged some nodes.
                changed |= !replaced.is_empty();

                // We've gotten the indices to be replaced with the current index,
                // the result of merging the current node with all of them, and the
                // nodes that weren't taken up in the merge.
                merged.push_back((curr_idx, merge_node));
                (merged, name_map) = replaced
                    .iter()
                    .fold((merged, name_map), |(nodes, name_map), from| {
                        rewrite_nodes(nodes, name_map, *from, curr_idx)
                    });

                op_nodes = unmerged
                    .into_iter()
                    .map(|(idx, node)| {
                        (
                            idx,
                            replaced
                                .iter()
                                .fold(node, |n, from| n.rewrite(*from, curr_idx)),
                        )
                    })
                    .collect();
            }

            // After we drain every value from op_nodes into merged, possibly
            // after merging, we reset op_nodes to merged and continue iterating.
            op_nodes = merged;
            merged = VecDeque::new();
        }

        Self {
            op_nodes: op_nodes.into_iter().collect(),
            name_map,
            all_fragments,
        }
    }

    fn parse_term_seq<'r>(&self, juxt: &'r [Expr]) -> Result<(Expr, &'r [Expr])> {
        if juxt.len() == 1 {
            return Ok((juxt[0].clone(), &[]));
        }

        //We want to take expressions from the input stream until we hit another
        // operator fragment.
        let next_fragment_idx = juxt
            .iter()
            .position(|i| {
                // Find the position in the input stream that is the
                // next fragment that we're aware of. We don't allow
                // overloading of operator fragments as variable
                // names (at the moment. Maybe never, might be a bad
                // idea).
                i.as_id_ref()
                    .map(|fragment| self.all_fragments.contains(fragment))
                    .unwrap_or(false)
            })
            // If we don't find a "next fragment", then the rest of
            // this input must be part of one function application.
            .unwrap_or(juxt.len());

        match next_fragment_idx {
            0 => Err(error_tree(
                "Got a fragment at head of input while trying to parse function application.",
                juxt.iter().map(|e| e.treeify()).collect(),
            )),
            1 => Ok((juxt[0].clone(), &juxt[1..])),
            _ => Ok((
                Expr::Appl {
                    head: Box::new(juxt[0].clone()),
                    args: juxt[1..next_fragment_idx].into_iter().cloned().collect(),
                },
                &juxt[next_fragment_idx..],
            )),
        }
    }

    /// Parse the ops that are stronger than the given operators from this
    /// precedence graph node.
    /// p↑ ::= V { q_hat | p < q } | closed+
    /// This rule also contains closed+ as part of its definition, due to 5.4
    fn parse_stronger_ops<'r>(
        &self,
        node: &OpNode,
        juxt: &'r [Expr],
    ) -> Result<(Expr, &'r [Expr])> {
        let stronger = node
            .stronger
            .iter()
            .map(|idx| &self.op_nodes[idx])
            .collect_vec();

        single_unique_result(
            "Parsing stronger operators",
            stronger
                .into_iter()
                .map(|op| self.parse_operators(op, juxt)),
            parse_to_err,
        )
        .or_else(|op_err| {
            self.parse_closed_plus(juxt).map_err(|fn_err| {
                error_tree(
                    "Compound failures parsing stronger ops",
                    vec![op_err, fn_err],
                )
            })
        })
    }

    fn parse_single_operator<'r, 'o>(
        &self,
        op: &'o OpSpec,
        juxt: &'r [Expr],
    ) -> Result<(&'o OpSpec, Vec<Expr>, &'r [Expr])> {
        let mut fragments = op.fragments.iter();
        // Get the first fragment out of the way.
        let first_symbol = fragments
            .next()
            .ok_or_else(|| error_tree("BUG: No head fragment for operator", vec![op.treeify()]))?;
        let mut juxt = match_fragment(first_symbol, juxt)?;

        let mut args = Vec::with_capacity(op.arity());
        for fragment in fragments {
            // Parse expr-then-fragment.
            let (arg, after) = self.parse_expr_inner(juxt)?;
            juxt = match_fragment(fragment, after)?;
            args.push(arg);
        }
        Ok((op, args, juxt))
    }

    /// This function parses the rule
    /// op_fix_p ::= V n1 expr n2 expr ··· nk | n1, ... , nk are the name parts of
    ///                                       | an operator in node `p` with
    ///                                       | fixity/associativity `fix`
    /// Which given the other rules means I'm pretty sure it's only meant to
    /// parse the interior arguments of the operator. The first and/or final
    /// operands are derived from the parse rule invoking this one(I think).
    fn parse_operator_fix<'r, 'o>(
        &self,
        node: &'o OpNode,
        juxt: &'r [Expr],
        fixity: Fixity,
    ) -> Result<(&'o OpSpec, Vec<Expr>, &'r [Expr])> {
        if juxt.len() == 0 {
            return Err(error(format!(
                "No input given to parse ops with fixity {}",
                fixity
            )));
        } else if juxt.len() == 1 {
            return Err(error(format!(
                "Cannot parse single expr {} as operator",
                juxt[0].treeify()
            )));
        }

        let filtered_ops: HashSet<_> = node.with_fixity(fixity).collect();

        if filtered_ops.is_empty() {
            return Err(error(format!(
                "No ops of fixity {fixity} in {}",
                node.op_set_label()
            )));
        }

        let results = filtered_ops
            .iter()
            .map(|op| self.parse_single_operator(op, juxt));

        single_unique_result(
            format!(
                "Parsing ops [{}]",
                filtered_ops.iter().map(|op| &op.name).join(", ")
            ),
            results,
            op_parse_to_err,
        )
    }

    /// Parse the rule closed+, where
    /// closed ::= identifier | ( expr ) | V { op_closed_p | p is a graph node }.
    /// This is the core of the modification for allowing this parser to handle
    /// function application. It handles bailing appropriately and converting
    /// the juxtaposed expressions into a function application.
    fn parse_closed_plus<'r>(&self, juxt: &'r [Expr]) -> Result<(Expr, &'r [Expr])> {
        let parse_single = |juxt| {
            single_unique_result(
                "Parsing all closed ops",
                self.op_nodes
                    .values()
                    .flat_map(|node| node.ops.values().filter(|op| op.fixity == Fixity::Closed))
                    .map(|op| self.parse_single_operator(op, juxt))
                    .map(|r| r.and_then(|(op, args, juxt)| op.as_expr(args).map(|e| (e, juxt)))),
                |p| parse_to_err(p),
            )
            .or_else(|e| {
                let fragment_is_next = juxt[0]
                    .as_id_ref()
                    .map(|sym| self.all_fragments.contains(sym))
                    .unwrap_or(false);

                if fragment_is_next {
                    Err(error_tree(
                        "Failed to parse closed op, but next item is a fragment",
                        std::iter::once(e)
                            .chain(juxt.iter().map(|t| t.treeify()))
                            .collect(),
                    ))
                } else {
                    Ok((juxt[0].clone(), &juxt[1..]))
                }
            })
        };

        let mut args = Vec::new();
        let mut juxt = juxt;
        while let Ok((val, rest)) = parse_single(juxt) {
            args.push(val);
            juxt = rest;
            if juxt.len() == 0 {
                break;
            }
        }

        match args.len() {
            0 => Err(error("No closed elements of juxt section")),
            1 => Ok((args[0].clone(), juxt)),
            _ => {
                let head = Box::new(args.remove(0));
                Ok((Expr::Appl { head, args }, juxt))
            }
        }
    }

    /// Parses the rule
    /// p_hat_non ::= p↑ op_non_p p↑
    fn parse_non<'r>(&self, node: &OpNode, juxt: &'r [Expr]) -> Result<(Expr, &'r [Expr])> {
        let (first, juxt) = self.parse_stronger_ops(node, juxt)?;

        let (winning_op, mut inner, juxt) =
            self.parse_operator_fix(node, juxt, Fixity::Infix(Assoc::Non))?;

        let (last, juxt) = self.parse_stronger_ops(node, juxt)?;

        inner.insert(0, first);
        inner.push(last);
        winning_op.as_expr(inner).map(|e| (e, juxt))
    }

    /// This function parses this rule.
    /// p_hat_right ::= (op_prefix_p | p↑ op_right_p) (p_hat_right | p↑)
    /// Which is equivalent to
    /// p_hat_right ::= op_prefix_p p_hat_right | op_prefix_p p↑ | p↑ op_right_p p_hat_right | p↑ op_right_p p↑
    fn parse_right<'r>(&self, node: &OpNode, juxt: &'r [Expr]) -> Result<(Expr, &'r [Expr])> {
        if juxt.len() == 1 {
            return Ok((juxt[0].clone(), &[]));
        }
        let (op, mut args, juxt) = single_unique_result(
            "p-right first element",
            vec![
                self.parse_operator_fix(node, juxt, Fixity::Prefix),
                self.parse_stronger_ops(node, juxt)
                    .and_then(|(first_arg, juxt)| {
                        let (op, mut args, juxt) =
                            self.parse_operator_fix(node, juxt, Fixity::Infix(Assoc::Right))?;

                        args.insert(0, first_arg);
                        Ok((op, args, juxt))
                    }),
            ]
            .into_iter(),
            op_parse_to_err,
        )?;

        let (last_arg, juxt) = self.parse_right(node, juxt).or_else(|op_err| {
            self.parse_stronger_ops(node, juxt).map_err(|fn_err| {
                error_tree("p-right last arg failed to parse", vec![op_err, fn_err])
            })
        })?;

        args.push(last_arg);
        op.as_expr(args).map(|e| (e, juxt))
    }

    /// The original grammar rule is this (but with more fancy script).
    /// p_hat_left ::= (p_hat_left | p↑) (op_postfix_p | op_left_p p↑)
    /// This is left-recursive, and a problem, but it looks pretty
    /// self-contained w.r.t the rest of the grammar, so I'm gonna do a
    /// thing I saw at https://www.geeksforgeeks.org/removing-direct-and-indirect-left-recursion-in-a-grammar/
    /// First, convert from CNF to DNF, because CNF here sounds like a pain
    /// in the ass.
    /// p_hat_left ::= p_hat_left op_postfix_p | p_hat_left op_left_p p↑ | p↑ op_postfix_p | p↑ op_left_p p↑
    /// Then we remove the direct left recursion by factoring
    /// p_hat_left ::== p↑ op_postfix_p p_hat_left' | p↑ op_left_p p↑ p_hat_left'
    /// p_hat_left' ::== op_postfix_p p_hat_left' | op_left_p p↑ p_hat_left' | e
    ///
    /// This is the function to parse this rule.
    /// p_hat_left ::== p↑ op_postfix_p p_hat_left'
    ///           | p↑ op_left_p p↑ p_hat_left'
    /// We can do some deduplicating in this rule.
    /// p_hat_left ::== p↑ (op_postfix_p | op_left_p p↑) p_hat_left'
    fn parse_left<'r>(&self, node: &OpNode, juxt: &'r [Expr]) -> Result<(Expr, &'r [Expr])> {
        // This is always going to be the left-most arg of the next node we try
        // to parse.
        let (first_arg, rest) = self
            .parse_stronger_ops(node, juxt)
            .map_err(|e| e.push_head("p_hat_left stronger op or term sequence".to_string()))?;

        // This uses the above arg, and once it has it serves as the left
        // argument for p_hat_left', if that parses successfully. If not, this
        // entire parse resolves to this operator application.
        let (op, mut args, rest) = single_unique_result(
            "p-left op parse",
            vec![
                self.parse_operator_fix(node, rest, Fixity::Postfix),
                self.parse_operator_fix(node, rest, Fixity::Infix(Assoc::Left))
                    .and_then(|(op, mut args, juxt)| {
                        let (last_arg, juxt) =
                            self.parse_stronger_ops(node, juxt).map_err(|e| {
                                e.push_head(
                                    "p_hat_left second stronger op or term sequence".to_string(),
                                )
                            })?;
                        args.push(last_arg);
                        Ok((op, args, juxt))
                    }),
            ]
            .into_iter(),
            op_parse_to_err,
        )?;
        args.insert(0, first_arg);
        let inner_arg = op.as_expr(args)?;

        self.parse_left_aux(node, rest, inner_arg)
    }

    /// This function parses this rule.
    /// p_hat_left' ::== oppostfixp p_hat_left'
    ///            | opleftp p↑ p_hat_left'
    ///            | e
    /// Merging some branches together, we get
    /// p_hat_left' ::== (oppostfixp | opleftp p↑) p_hat_left'
    ///            | e
    fn parse_left_aux<'r>(
        &self,
        node: &OpNode,
        juxt: &'r [Expr],
        left_hand: Expr,
    ) -> Result<(Expr, &'r [Expr])> {
        let first = self.parse_operator_fix(node, juxt, Fixity::Postfix);
        let second = self
            .parse_operator_fix(node, juxt, Fixity::Infix(Assoc::Left))
            .and_then(|(op, mut args, juxt)| {
                let (last_arg, juxt) = single_unique_result(
                    "p_hat_left-aux stronger op or term sequence",
                    vec![
                        self.parse_stronger_ops(node, juxt),
                        self.parse_term_seq(juxt),
                    ]
                    .into_iter(),
                    parse_to_err,
                )?;
                args.push(last_arg);
                Ok((op, args, juxt))
            });

        if first.is_err() && second.is_err() {
            // This rule is allowed to fail, and the base case is to return the
            // left hand argument.
            Ok((left_hand, juxt))
        } else {
            // At least one parse succeeded, so we need a unique parse, and then
            // we need to do the appropriate alterations to the operator args.
            let (op, mut args, rest) = single_unique_result(
                "p-left aux parse",
                vec![first, second].into_iter(),
                &op_parse_to_err,
            )?;
            args.insert(0, left_hand);
            self.parse_left_aux(node, rest, op.as_expr(args)?)
        }
    }

    /// Parses the rule:
    /// p_hat ::= p_hat_non | p_hat_right | p_hat_left
    /// We've removed p_hat_closed to comply with section 5.4's modification of the
    /// grammar for function application by juxtaposition.
    fn parse_operators<'r>(&self, node: &OpNode, juxt: &'r [Expr]) -> Result<(Expr, &'r [Expr])> {
        let results = vec![
            self.parse_non(node, juxt),
            self.parse_right(node, juxt),
            self.parse_left(node, juxt),
        ];

        single_unique_result(
            format!("op-set {}", node.op_set_label()),
            results.into_iter(),
            parse_to_err,
        )
    }

    /// This is the start of the recursive bit of our parser. It expects to
    /// potentially leave some of the input stream left.
    /// expr ::= V { p_hat | p is a graph node } | closed+
    /// We also add in the `closed+` branch due to section 5.4's modifications.
    fn parse_expr_inner<'r>(&self, juxt: &'r [Expr]) -> Result<(Expr, &'r [Expr])> {
        match juxt.len() {
            0 => Err(error("Tried to parse empty expression list")),
            1 => Ok((juxt[0].clone(), &[])),
            _ => {
                let op_results = self
                    .op_nodes
                    .values()
                    .map(|node| self.parse_operators(node, juxt));

                single_unique_result("inner expression list", op_results, parse_to_err).or_else(
                    |op_err| {
                        self.parse_closed_plus(juxt).map_err(|fn_err| {
                            error_tree("Inner Expr parse failures", vec![op_err, fn_err])
                        })
                    },
                )
            }
        }
    }

    pub fn parse_expr(&self, juxt: &[Expr]) -> Result<Expr> {
        // Attempt to parse every operator we know about.
        let (parse, juxt) = self.parse_expr_inner(juxt)?;

        // There should be nothing left after parsing.
        if juxt.len() != 0 {
            return Err(error_tree(
                "Didn't completely consume exprs after parsing expr.",
                vec![
                    parse.treeify().map_head(&|h| format!("Parsed: {h}")),
                    error_tree(
                        "Remaining",
                        juxt.iter().map(|expr| expr.treeify()).collect(),
                    ),
                ],
            ));
        }
        Ok(parse)
    }
}

impl Default for PrecedenceGraph {
    fn default() -> Self {
        match PrecedenceGraph::try_new(default_ops()) {
            Ok(r) => r,
            Err(e) => panic!("Failed to parse default operator set: {}", e.fmt_tree()),
        }
    }
}

impl TreeFormat for PrecedenceGraph {
    fn treeify(&self) -> Tree<String> {
        error_tree(
            "Precedence graph",
            self.op_nodes
                .iter()
                .sorted_by(|(l, _), (r, _)| l.cmp(r))
                .map(|(idx, node)| node.treeify().map_head(&|h| format!("#{idx} {h}")))
                .collect(),
        )
    }
}

#[cfg(test)]
mod test {

    use num::{rational::Ratio, BigInt};
    use rosary::format::TreeFormat;

    use crate::{
        abstract_syntax::{literal::Literal, AbstractSyntax, Expr},
        concrete_syntax::{symbols::Symbol, ConcreteSyntax},
    };

    #[test]
    pub fn addition_expr() {
        let want = Expr::Appl {
            head: Box::new(
                Expr::from_concrete(Symbol::test_parse("add").into())
                    .expect("Failed to parse test symbol"),
            ),
            args: vec![
                Expr::Literal(Literal::Num(Ratio::from(BigInt::from(3)))),
                Expr::Literal(Literal::Num(Ratio::from(BigInt::from(4)))),
            ],
        };

        let got = Expr::test_parse("3 + 4");
        assert_eq!(
            got,
            want,
            "\nGot {}\n=====\nWant {}",
            got.treeify(),
            want.treeify()
        );
    }

    #[test]
    pub fn two_lits_expr() {
        let got = Expr::test_parse("Foo Bar -> Baz");
        let want = Expr::Appl {
            head: Box::new(Expr::test_parse("arrow")),
            args: vec![Expr::test_parse("Foo Bar"), Expr::test_parse("Baz")],
        };

        assert_eq!(
            got,
            want,
            "\nGot {}\n=====\nWant {}",
            got.treeify(),
            want.treeify()
        );
    }

    #[test]
    pub fn litapp_expr() {
        let got = Expr::test_parse("Foo (Bar 1) -> Baz");
        let want = Expr::Appl {
            head: Box::new(Expr::test_parse("arrow")),
            args: vec![Expr::test_parse("Foo (Bar 1)"), Expr::test_parse("Baz")],
        };

        assert_eq!(
            got,
            want,
            "\nGot {}\n=====\nWant {}",
            got.treeify(),
            want.treeify()
        );
    }

    #[test]
    pub fn one_arrow_expr() {
        let got = Expr::test_parse("Foo -> Bar");
        let want = Expr::Appl {
            head: Box::new(Expr::test_parse("arrow")),
            args: vec![Expr::test_parse("Foo"), Expr::test_parse("Bar")],
        };

        assert_eq!(
            got,
            want,
            "\nGot {}\nWant {}",
            got.treeify(),
            want.treeify()
        );
    }

    #[test]
    pub fn chained_arrow_expr() {
        let got = Expr::test_parse("Foo -> Bar -> Baz");

        let want = Expr::Appl {
            head: Box::new(Expr::test_parse("arrow")),
            args: vec![Expr::test_parse("Foo"), Expr::test_parse("Bar -> Baz")],
        };

        assert_eq!(
            got,
            want,
            "\nGot {}\nWant {}",
            got.treeify(),
            want.treeify()
        );
    }

    #[test]
    fn add_div_expr() {
        let got = Expr::test_parse("3 + 4 / 9");
        let want = Expr::Appl {
            head: Box::new(Expr::test_parse("add")),
            args: vec![Expr::test_parse("3"), Expr::test_parse("4 / 9")],
        };

        assert_eq!(
            got,
            want,
            "\nGot {}\nWant {}",
            got.treeify(),
            want.treeify()
        );
    }
}
