use std::{convert::TryFrom, iter};

use rosary::{format::TreeFormat, vec_tree::Tree};

use crate::{
    abstract_syntax::{expr::Expr, literal::Literal, AbstractSyntax},
    concrete_syntax::{
        error,
        pattern::Pattern as ConcretePattern,
        symbols::{QualName, Symbol},
    },
    result::{self, Result},
};

#[derive(Debug, Clone, PartialEq)]
pub enum Pattern<Id> {
    Literal(Literal<Id>),
    Tuple(Vec<Self>),
    List(Vec<Self>),
    Appl { head: Box<Self>, args: Vec<Self> },
}

impl AbstractSyntax<ConcretePattern> for Pattern<Symbol> {
    fn from_concrete(source: ConcretePattern) -> Result<Pattern<Symbol>> {
        match source {
            ConcretePattern::Literal(lit) => Literal::from_concrete(lit).map(Self::Literal),
            ConcretePattern::Tuple(tuple) => result::list(
                "failed to convert tuple elements to patterns",
                tuple
                    .0
                    .into_iter()
                    .map(|expr| Expr::from_concrete(expr).and_then(Pattern::try_from)),
                |s| s.treeify(),
            )
            .map(Self::Tuple),
            ConcretePattern::List(list) => result::list(
                "failed to convert list elements to patterns",
                list.0
                    .into_iter()
                    .map(|expr| Expr::from_concrete(expr).and_then(Pattern::try_from)),
                |s| s.treeify(),
            )
            .map(Self::List),
            ConcretePattern::Juxtaposed(j) => result::list(
                "Juxtaposed Patterns",
                j.into_iter().map(|p| Self::from_concrete(p)),
                |s| s.treeify(),
            )
            .and_then(|mut patterns| match patterns.len() {
                0 => Err(error("Got an empty juxtaposed pattern list?")),
                1 => Ok(patterns.remove(0)),
                _ => Ok(Pattern::Appl {
                    head: Box::new(patterns.remove(0)),
                    args: patterns,
                }),
            }),
        }
    }
}

impl<Id: ToString> TreeFormat for Pattern<Id> {
    fn treeify(&self) -> rosary::vec_tree::Tree<String> {
        match self {
            Pattern::Literal(l) => l.treeify(),
            Pattern::Tuple(t) => {
                Tree::Branch("Tuple".to_string(), t.iter().map(|p| p.treeify()).collect())
            }
            Pattern::List(l) => {
                Tree::Branch("List".to_string(), l.iter().map(|p| p.treeify()).collect())
            }
            Pattern::Appl { head, args } => Tree::Branch(
                "Application".to_string(),
                iter::once(Tree::Branch("Head".to_string(), vec![head.treeify()]))
                    .chain(args.iter().map(|p| p.treeify()))
                    .collect(),
            ),
        }
    }
}

impl<Id: ToString> TryFrom<Expr<Id>> for Pattern<Id> {
    type Error = Tree<String>;

    fn try_from(value: Expr<Id>) -> Result<Self> {
        match value {
            Expr::Literal(lit) => Ok(Self::Literal(lit)),
            Expr::List(list) => result::list(
                "Converting list expr to pattern",
                list.into_iter().map(Pattern::try_from),
                |p| p.treeify(),
            )
            .map(Self::List),
            Expr::Tuple(tuple) => result::list(
                "Converting tuple expr to pattern",
                tuple.into_iter().map(Pattern::try_from),
                |p| p.treeify(),
            )
            .map(Self::Tuple),
            Expr::Lambda { params, body } => Pattern::try_from(*body).map(|head| Pattern::Appl {
                head: Box::new(head),
                args: params,
            }),
            Expr::Appl { head, args } => {
                let head = Pattern::try_from(*head);
                let args = result::list(
                    "Parsing application args",
                    args.into_iter().map(Pattern::try_from),
                    |p| p.treeify(),
                );

                match (head, args) {
                    (Ok(head), Ok(args)) => Ok(Self::Appl {
                        head: Box::new(head),
                        args,
                    }),
                    (Ok(head), Err(e)) => Err(Tree::Branch(
                        "Failed converting application args".to_string(),
                        vec![head.treeify(), e],
                    )),
                    (Err(e), Ok(args)) => Err(Tree::Branch(
                        "Failed converting application head".to_string(),
                        vec![
                            e,
                            Tree::Branch(
                                "Args".to_string(),
                                args.into_iter().map(|a| a.treeify()).collect(),
                            ),
                        ],
                    )),
                    (Err(he), Err(ae)) => Err(Tree::Branch(
                        "Failed converting application head and args".to_string(),
                        vec![he, ae],
                    )),
                }
            }
            e => Err(e.treeify()),
        }
    }
}
