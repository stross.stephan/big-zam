use pest::iterators::Pair;
use rosary::{format::TreeFormat, vec_tree::Tree};

use crate::{
    concrete_syntax::{drop_next, expr::Expr, pattern::Pattern, ConcreteSyntax, Rule},
    result::Result,
};

#[derive(Debug, Clone, PartialEq)]
pub struct Lambda {
    pub args: Vec<Pattern>,
    pub body: Expr,
}

impl ConcreteSyntax for Lambda {
    const EXPECTED_RULE: Rule = Rule::lambda_expr;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let mut pairs = pair.into_inner();

        drop_next(&mut pairs, Rule::FN_KW)?;
        let args = {
            let mut args = Pattern::parse_many(&mut pairs)?;
            if args.len() == 1 {
                match args.remove(0) {
                    Pattern::Juxtaposed(inner) => inner,
                    p => vec![p],
                }
            } else {
                args
            }
        };

        drop_next(&mut pairs, Rule::THICK_ARROW)?;
        let body = Expr::parse_next("Parsing lambda body", &mut pairs)?;

        Ok(Self { args, body })
    }
}

impl TreeFormat for Lambda {
    fn treeify(&self) -> Tree<String> {
        {
            let arg_tree = Tree::Branch(
                "Args".to_string(),
                self.args.iter().map(|a| a.treeify()).collect(),
            );
            Tree::Branch("Function".to_string(), vec![arg_tree, self.body.treeify()])
        }
    }
}

#[cfg(test)]
mod test {}
