use pest::iterators::Pair;
use rosary::{format::TreeFormat, vec_tree::Tree};

use crate::{
    concrete_syntax::{
        branches::match_expr::{match_arm_block::MatchArmBlock, match_arm_expr::MatchArmExpr},
        drop_next, error,
        expr::Expr,
        maybe_drop_next,
        pattern::Pattern,
        ConcreteSyntax, Rule,
    },
    result::Result,
};

#[derive(Debug, Clone, PartialEq)]
pub enum MatchArmLeading {
    Block(MatchArmBlock, bool),
    Expr(MatchArmExpr),
}

impl MatchArmLeading {
    pub fn into_inner(self) -> (Pattern, Expr) {
        match self {
            MatchArmLeading::Block(arm, _) => arm.into_inner(),
            MatchArmLeading::Expr(arm) => arm.into_inner(),
        }
    }
}

impl ConcreteSyntax for MatchArmLeading {
    const EXPECTED_RULE: Rule = Rule::match_arm_leading;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let mut pairs = pair.into_inner();
        let body_pair = pairs.next().ok_or_else(|| error("No body"))?;
        match body_pair.as_rule() {
            Rule::match_arm_block => {
                let block = MatchArmBlock::parse_pair(body_pair)?;
                let tailing_sep = maybe_drop_next(&mut pairs, Rule::SEQ_SEP)?;
                Ok(Self::Block(block, tailing_sep))
            }
            Rule::match_arm_expr => {
                let expr = MatchArmExpr::parse_pair(body_pair)?;
                drop_next(&mut pairs, Rule::SEQ_SEP)?;
                Ok(Self::Expr(expr))
            }
            rule => {
                Err(format!("Expected leading match arm, got unexpected body {:?}", rule).into())
            }
        }
    }
}

impl TreeFormat for MatchArmLeading {
    fn treeify(&self) -> Tree<String> {
        match self {
            MatchArmLeading::Block(block, _) => block.treeify(),
            MatchArmLeading::Expr(expr) => expr.treeify(),
        }
    }
}
