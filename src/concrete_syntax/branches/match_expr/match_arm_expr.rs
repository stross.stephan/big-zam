use pest::iterators::Pair;
use rosary::{format::TreeFormat, vec_tree::Tree};

use crate::{
    concrete_syntax::{drop_next, expr::Expr, pattern::Pattern, ConcreteSyntax, Rule},
    result::Result,
};

#[derive(Debug, Clone, PartialEq)]
pub struct MatchArmExpr {
    pub pattern: Pattern,
    pub expr: Expr,
}

impl MatchArmExpr {
    pub fn into_inner(self) -> (Pattern, Expr) {
        let Self { pattern, expr } = self;
        (pattern, expr)
    }
}

impl ConcreteSyntax for MatchArmExpr {
    const EXPECTED_RULE: Rule = Rule::match_arm_expr;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let mut pairs = pair.into_inner();

        let pattern = Pattern::parse_next("Missing Pattern", &mut pairs)?;
        drop_next(&mut pairs, Rule::THICK_ARROW)?;
        let expr = Expr::parse_next("Missing Expression", &mut pairs)?;

        Ok(Self { pattern, expr })
    }
}

impl TreeFormat for MatchArmExpr {
    fn treeify(&self) -> Tree<String> {
        let head = format!("Match Arm Expr");
        let children = vec![self.pattern.treeify(), self.expr.treeify()];
        Tree::Branch(head, children)
    }
}
