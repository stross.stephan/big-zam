use pest::iterators::Pair;
use rosary::{format::TreeFormat, vec_tree::Tree};

use crate::{
    concrete_syntax::{
        branches::match_expr::{match_arm_block::MatchArmBlock, match_arm_expr::MatchArmExpr},
        error,
        expr::Expr,
        maybe_drop_next,
        pattern::Pattern,
        ConcreteSyntax, Rule,
    },
    result::Result,
};

#[derive(Debug, Clone, PartialEq)]
pub enum MatchArmLast {
    Block(MatchArmBlock, bool),
    Expr(MatchArmExpr, bool),
}

impl MatchArmLast {
    pub fn into_inner(self) -> (Pattern, Expr) {
        match self {
            MatchArmLast::Block(arm, _) => arm.into_inner(),
            MatchArmLast::Expr(arm, _) => arm.into_inner(),
        }
    }
}

impl ConcreteSyntax for MatchArmLast {
    const EXPECTED_RULE: Rule = Rule::match_arm_last;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let mut pairs = pair.into_inner();
        let arm_pair = pairs.next().ok_or_else(|| error("No last arm"))?;
        match arm_pair.as_rule() {
            Rule::match_arm_block => {
                let v = MatchArmBlock::parse_pair(arm_pair)?;
                let sep = maybe_drop_next(&mut pairs, Rule::SEQ_SEP)?;
                Ok(MatchArmLast::Block(v, sep))
            }
            Rule::match_arm_expr => {
                let v = MatchArmExpr::parse_pair(arm_pair)?;
                let sep = maybe_drop_next(&mut pairs, Rule::SEQ_SEP)?;
                Ok(MatchArmLast::Expr(v, sep))
            }
            rule => {
                return Err(format!(
                    "Got unexpected rule {:?} instead of expected arm type.",
                    rule
                )
                .into())
            }
        }
    }
}

impl TreeFormat for MatchArmLast {
    fn treeify(&self) -> Tree<String> {
        match self {
            MatchArmLast::Block(block, _) => block.treeify(),
            MatchArmLast::Expr(expr, _) => expr.treeify(),
        }
    }
}
