use pest::iterators::Pair;
use rosary::{format::TreeFormat, vec_tree::Tree};

use crate::{
    concrete_syntax::{
        drop_next,
        expr::{Expr, InnerExpr},
        pattern::Pattern,
        sequences::Block,
        ConcreteSyntax, Rule,
    },
    result::Result,
};

#[derive(Debug, Clone, PartialEq)]
pub struct MatchArmBlock {
    pattern: Pattern,
    value: Block,
}

impl MatchArmBlock {
    pub fn into_inner(self) -> (Pattern, Expr) {
        let Self { pattern, value } = self;
        (pattern, InnerExpr::from(value).into())
    }
}

impl ConcreteSyntax for MatchArmBlock {
    const EXPECTED_RULE: Rule = Rule::match_arm_block;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let mut pairs = pair.into_inner();
        let pattern = Pattern::parse_next("Missing Pattern", &mut pairs)?;
        drop_next(&mut pairs, Rule::THICK_ARROW)?;
        let value = Block::parse_next("Missing Block", &mut pairs)?;

        Ok(Self { pattern, value })
    }
}

impl TreeFormat for MatchArmBlock {
    fn treeify(&self) -> Tree<String> {
        let head = format!("Match Arm Block");
        let children = vec![self.pattern.treeify(), self.value.treeify()];
        Tree::Branch(head, children)
    }
}
