use itertools::Itertools;
use pest::iterators::Pair;
use rosary::{format::TreeFormat, vec_tree::Tree};

use crate::{
    concrete_syntax::{
        branches::match_expr::{match_arm_last::MatchArmLast, match_arm_leading::MatchArmLeading},
        drop_next, error_tree,
        expr::Expr,
        pattern::Pattern,
        ConcreteSyntax, Rule,
    },
    result::Result,
};

mod match_arm_block;
mod match_arm_expr;
mod match_arm_last;
mod match_arm_leading;

#[derive(Debug, Clone, PartialEq)]
pub struct Match {
    pub matched_value: Expr,
    pub leading: Vec<MatchArmLeading>,
    pub last: Option<MatchArmLast>,
}

impl Match {
    pub fn into_inner(self) -> (Expr, Vec<(Pattern, Expr)>) {
        let Self {
            matched_value,
            leading,
            last,
        } = self;

        let mut arms = leading
            .into_iter()
            .map(|arm| arm.into_inner())
            .collect::<Vec<_>>();
        if let Some(last) = last {
            arms.push(last.into_inner());
        }

        (matched_value, arms)
    }
}

impl ConcreteSyntax for Match {
    const EXPECTED_RULE: Rule = Rule::match_expr;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let mut pairs = pair.into_inner();

        drop_next(&mut pairs, Rule::MATCH_KW)?;
        let matched_value = Expr::parse_next("Missing discriminant", &mut pairs)?;
        drop_next(&mut pairs, Rule::LCURLY)?;

        let (leading, mut errs) = pairs
            .take_while_ref(|pair| pair.as_rule() == Rule::match_arm_leading)
            .map(|pair| MatchArmLeading::parse_pair(pair))
            .partition_result::<Vec<_>, Vec<_>, _, _>();

        let last = match MatchArmLast::parse_next_if_matches(&mut pairs) {
            Ok(last) => last,
            Err(e) => {
                errs.push(e);
                None
            }
        };

        if errs.len() > 0 {
            return Err(error_tree("Got arm parsing errors", errs));
        }

        Ok(Self {
            matched_value,
            leading,
            last,
        })
    }
}

impl TreeFormat for Match {
    fn treeify(&self) -> Tree<String> {
        let Self {
            matched_value,
            leading,
            last,
        } = self;

        let mut children = vec![Tree::Branch(
            "Matched Expr".to_string(),
            vec![matched_value.treeify()],
        )];

        let arm_trees = leading
            .iter()
            .map(|l| l.treeify())
            .chain(std::iter::once(last.as_ref().map(|l| l.treeify())).filter_map(|t| t))
            .collect::<Vec<_>>();

        if arm_trees.len() == 0 {
            children.push("No Arms".to_string().into())
        } else {
            children.push(Tree::Branch("Arms".to_string(), arm_trees))
        }

        Tree::Branch("Match".to_string(), children)
    }
}

#[cfg(test)]
mod test {
    use crate::concrete_syntax::{
        branches::match_expr::match_arm_expr::MatchArmExpr, pattern::Pattern,
    };

    use super::*;

    #[test]
    fn empty_match_str() {
        let test_str = r#"match name {

}"#;

        let got = Match::test_parse(test_str);
        let want = Match {
            matched_value: Expr::test_parse("name"),
            leading: vec![],
            last: None,
        };

        assert_eq!(got, want);
    }

    #[test]
    fn single_arm() {
        let want = Match {
            matched_value: Expr::test_parse("name"),
            leading: vec![],
            last: Some(MatchArmLast::Expr(
                MatchArmExpr {
                    pattern: Pattern::test_parse("inner_pat"),
                    expr: Expr::test_parse("(some expr)"),
                },
                false,
            )),
        };

        let test_str = r#"match name {
    inner_pat => (some expr)
}"#;
        let got = Match::test_parse(test_str);

        assert_eq!(got, want)
    }

    #[test]
    fn double_arm() {
        let test_str = r#"match name {
            inner_1 => (some expr),
            inner_2 => (other val)
        }"#;

        let got = Match::test_parse(test_str);
        let want = Match {
            matched_value: Expr::test_parse("name"),
            leading: vec![MatchArmLeading::Expr(MatchArmExpr {
                pattern: Pattern::test_parse("inner_1"),
                expr: Expr::test_parse("(some expr)"),
            })],
            last: Some(MatchArmLast::Expr(
                MatchArmExpr {
                    pattern: Pattern::test_parse("inner_2"),
                    expr: Expr::test_parse("(other val)"),
                },
                false,
            )),
        };

        assert_eq!(got, want);
    }
}
