use strum_macros::Display;

#[derive(Debug, Display)]
pub enum Error {
    UnrecognizedSign(String),
    UnrecognizedToken,
    EndOfTokenStream,
    Parse1Token,
}

impl std::error::Error for Error {}
