use std::collections::HashMap;

use camino::{Utf8Path, Utf8PathBuf};
use itertools::{Either, Itertools};

use pest::iterators::Pair;
use rosary::{format::TreeFormat, vec_tree::Tree};

use crate::{
    concrete_syntax::{
        comments::Comment,
        error_tree, fmt_pair,
        statements::Stmt,
        symbols::{QualName, Symbol},
        ConcreteSyntax, Rule,
    },
    result::Result,
};

fn canonical_path_module<P: AsRef<Utf8Path>>(path: P) -> Result<(Symbol, P)> {
    let path_ref = path
        .as_ref()
        .canonicalize_utf8()
        .map_err(|e| Tree::Leaf(e.to_string()))?;

    let name = path_ref
        .file_name()
        .ok_or_else(|| format!("Path {path_ref} does not have a file name."))?;
    let name = Symbol::parse_str(name).or_else(|e| {
        Err(error_tree(
            format!("Failed to parse file name {path_ref} as a valid symbol.",),
            vec![e],
        ))
    })?;

    match (path_ref.is_file(), path_ref.extension()) {
        (true, Some("zam")) => Ok((name, path)),
        (true, Some(invalid)) => Err(format!(
            "File extension of `{path_ref}` does not match `zam`, and must.\nActual: {invalid}"
        )
        .into()),
        (true, None) => Err(format!(
            "File `{path_ref}` has no extension, but zam modules must end in `.zam`"
        )
        .into()),
        (false, _) => Err(format!(
            "Path `{path_ref}` is not a valid `*.zam` file, please point the \
             compiler at a file named `{path_ref}.zam` to have the contents of \
             that directory read as child modules."
        )
        .into()),
    }
}

fn child_module_paths<P: AsRef<Utf8Path>>(path: P) -> Result<Vec<Utf8PathBuf>> {
    let dir_path = path.as_ref().with_extension("");
    if !dir_path.is_dir() {
        // No child paths if it isn't a directory path.
        return Ok(Vec::with_capacity(0));
    }
    let (entries, errs): (Vec<_>, Vec<_>) = dir_path
        .read_dir_utf8()
        .map_err(|e| Tree::Leaf(e.to_string()))?
        .map(|dir_result| dir_result.map(|entry| entry.path().to_owned()))
        .partition_result();
    if !errs.is_empty() {
        return Err(error_tree(
            format!("Got errors reading {} entries", path.as_ref()),
            errs.into_iter().map(|e| e.to_string()).collect(),
        ));
    }
    Ok(entries)
}

#[derive(Debug)]
pub struct ZamFile {
    pub header_comment: Option<Comment>,
    pub body: Vec<Either<Stmt, Comment>>,
    pub footer_comment: Option<Comment>,
    pub children: HashMap<Symbol, Self>,
}

impl ZamFile {
    /// A path fed into this function should have a final component of the
    /// format `some_module_name.zam`. Receiving a file of that type will look
    /// for a folder called `some_module_name` in the same directory to contain
    /// the child modules of the passed file. Each zam file in that directory
    /// will be given the same treatment. If no folder is found, the module is
    /// considered to have no children (at least, none inferred from the
    /// filesystem). If you instead pass the path to the folder
    /// `some_module_name` directly, it will look for `some_module_name.zam` and
    /// error if it cannot find it.
    pub fn from_path<P: AsRef<Utf8Path>>(path: P) -> Result<(Symbol, Self)> {
        let (mod_name, mod_file) = canonical_path_module(&path)?;
        let this = ZamFile::parse_str(
            &std::fs::read_to_string(mod_file.as_ref())
                .map_err(|e| format!("Failure when reading file {}:\n{e}", mod_file.as_ref()))?,
        )?;

        let (children, child_errs): (HashMap<_, _>, Vec<_>) = child_module_paths(&path)?
            .into_iter()
            .map(|child_path| Self::from_path(&child_path))
            .partition_result();

        if child_errs.len() > 0 {
            Err(error_tree(
                format!("Errors in child modules of {}", mod_file.as_ref()),
                child_errs,
            ))
        } else {
            Ok((mod_name.into(), ZamFile { children, ..this }))
        }
    }

    pub fn bound_names(&self) -> () {}
}

impl ConcreteSyntax for ZamFile {
    const EXPECTED_RULE: Rule = Rule::module;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let mut body = pair
            .into_inner()
            .take_while(|pair| pair.as_rule() != Rule::EOI)
            .map(|pair| match pair.as_rule() {
                Rule::comment => Comment::parse_pair(pair).map(Either::Right),
                Rule::stmt => Stmt::parse_pair(pair).map(Either::Left),
                _ => {
                    return Err(error_tree(
                        "Unknown pair encountered before EOI in file.",
                        vec![fmt_pair(&pair)],
                    ))
                }
            })
            .collect::<Result<Vec<_>>>()?;

        let header_comment = if body.get(0).map(|v| v.is_right()).unwrap_or(false) {
            body.remove(0).right()
        } else {
            None
        };

        let footer_comment = if body
            .get(body.len() - 1)
            .map(|v| v.is_right())
            .unwrap_or(false)
        {
            body.remove(body.len() - 1).right()
        } else {
            None
        };

        Ok(ZamFile {
            header_comment,
            footer_comment,
            body,
            children: HashMap::with_capacity(0),
        })
    }
}

impl TreeFormat for ZamFile {
    fn treeify(&self) -> Tree<String> {
        let mut leaves = Vec::with_capacity(4);
        self.header_comment
            .as_ref()
            .map(|c| leaves.push(c.treeify().swap_head("Header Comment".to_string()).1));

        if !self.body.is_empty() {
            leaves.push(Tree::Branch(
                "Body".to_string(),
                self.body
                    .iter()
                    .map(|elem| match elem {
                        Either::Left(stmt) => stmt.treeify(),
                        Either::Right(comment) => comment.treeify(),
                    })
                    .collect(),
            ));
        }

        self.footer_comment
            .as_ref()
            .map(|c| leaves.push(c.treeify().swap_head("Footer Comment".to_string()).1));

        if !self.children.is_empty() {
            leaves.push(Tree::Branch(
                "Children".to_string(),
                self.children
                    .iter()
                    .map(|(name, module)| module.treeify().swap_head(name.to_string()).1)
                    .collect(),
            ));
        }

        Tree::Branch("Base Module".to_string(), leaves)
    }
}
