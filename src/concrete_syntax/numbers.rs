use std::fmt::Display;

use itertools::Itertools;

use pest::iterators::{Pair, Pairs};
use rosary::{format::TreeFormat, vec_tree::Tree};

use crate::{
    concrete_syntax::{error_tree, ConcreteSyntax, Rule},
    result::Result,
};

#[derive(PartialEq, Eq, Debug, Copy, Clone, Hash)]
pub enum Sign {
    Positive,
    Negative,
}

impl ConcreteSyntax for Sign {
    const EXPECTED_RULE: Rule = Rule::SIGN;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        match pair.as_str() {
            "+" => Ok(Sign::Positive),
            "-" => Ok(Sign::Negative),
            wat => Err(format!("Unrecognized sign parsed: {wat}").into()),
        }
    }
}

impl Display for Sign {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let sign = match self {
            Sign::Positive => '+',
            Sign::Negative => '-',
        };
        write!(f, "{sign}")
    }
}

impl Default for Sign {
    fn default() -> Self {
        Self::Positive
    }
}

impl From<&Sign> for char {
    fn from(val: &Sign) -> Self {
        match val {
            Sign::Positive => '+',
            Sign::Negative => '-',
        }
    }
}

#[derive(PartialEq, Eq, Debug, Copy, Clone, Hash)]
pub enum NumBase {
    Binary,
    Octal,
    Decimal,
    Hexadecimal,
    Base64,
}

impl NumBase {
    pub fn radix(&self) -> u32 {
        match self {
            NumBase::Binary => 2,
            NumBase::Octal => 8,
            NumBase::Decimal => 10,
            NumBase::Hexadecimal => 16,
            NumBase::Base64 => 64,
        }
    }
}

impl Display for NumBase {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let base = match self {
            NumBase::Binary => "0b",
            NumBase::Octal => "0o",
            NumBase::Decimal => "0d",
            NumBase::Hexadecimal => "0x",
            NumBase::Base64 => "64b",
        };
        write!(f, "{base}")
    }
}

impl Default for NumBase {
    fn default() -> Self {
        Self::Decimal
    }
}

#[derive(PartialEq, Eq, Hash, Debug, Clone)]
pub enum NumSeq {
    /// Requires a `has_dot` field because a decimal doesn't necessarily appear
    /// in this variant, but it must be present in the other two variants.
    WholeOnly {
        has_dot: bool,
        text: String,
    },
    BothParts {
        whole: String,
        frac: String,
    },
    FracOnly(String),
}

impl NumSeq {
    fn parse_rational(dropping_base: bool, pairs: Pairs<Rule>) -> Result<NumSeq> {
        let mut pairs = if dropping_base {
            pairs.dropping(1)
        } else {
            pairs
        };
        let whole = pairs
            .next()
            .ok_or_else(|| Tree::Leaf("Not enough pairs when parsing rational".to_string()))?
            .as_str()
            .to_string();
        let frac = pairs
            .skip(1)
            .next()
            .ok_or_else(|| Tree::Leaf("Not enough pairs when parsing rational".to_string()))?
            .as_str()
            .to_string();
        Ok(NumSeq::BothParts { whole, frac })
    }
    fn parse_whole(dropping_base: bool, pairs: Pairs<Rule>) -> Result<NumSeq> {
        let mut pairs = if dropping_base {
            pairs.dropping(1)
        } else {
            pairs
        };
        // There's at least one pair in `*_whole` rules.
        let text = pairs
            .next()
            .ok_or_else(|| Tree::Leaf("No pairs given when parsing whole.".to_string()))?
            .as_str()
            .to_string();
        let has_dot = pairs.next().is_some();
        Ok(NumSeq::WholeOnly { has_dot, text })
    }
    fn parse_frac(dropping_base: bool, pairs: Pairs<Rule>) -> Result<NumSeq> {
        let pairs = if dropping_base {
            pairs.dropping(1)
        } else {
            pairs
        };
        let frac = pairs.skip(1).next().unwrap().as_str().to_string();
        Ok(NumSeq::FracOnly(frac))
    }
}

impl Display for NumSeq {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            NumSeq::WholeOnly { has_dot, text } => {
                write!(f, "{text}")?;
                if *has_dot {
                    write!(f, ".")
                } else {
                    Ok(())
                }
            }
            NumSeq::BothParts { whole, frac } => write!(f, "{whole}.{frac}"),
            NumSeq::FracOnly(frac) => write!(f, ".{frac}"),
        }
    }
}

#[derive(PartialEq, Eq, Hash, Debug, Clone)]
pub struct Number {
    pub base: Option<NumBase>,
    pub sign: Option<Sign>,
    pub digits: NumSeq,
}

impl NumBase {
    fn digit_set(&self) -> &'static str {
        match self {
            Self::Binary => "_01",
            Self::Octal => "_01234567",
            Self::Decimal => "_0123456789",
            Self::Hexadecimal => "_0123456789abcdefABCDEF",
            Self::Base64 => {
                "=0123456789abcdefghijklmnopqrstu\
                 vwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+/"
            }
        }
    }

    fn base_name(&self) -> &'static str {
        match self {
            NumBase::Binary => "Binary",
            NumBase::Octal => "Octal",
            NumBase::Decimal => "Decimal",
            NumBase::Hexadecimal => "Hexadecimal",
            NumBase::Base64 => "Base64",
        }
    }
}

impl Number {
    fn char_to_val(ch: char, base: NumBase) -> Result<Option<u32>> {
        let val = match (ch, base) {
            ('_', _) => None, // Universal spacer
            ('0', NumBase::Binary | NumBase::Octal | NumBase::Decimal | NumBase::Hexadecimal) => {
                Some(0)
            }
            ('1', NumBase::Binary | NumBase::Octal | NumBase::Decimal | NumBase::Hexadecimal) => {
                Some(1)
            }
            (c, NumBase::Binary) => {
                return Err(format!("Got invalid char {c} in binary string.").into())
            }
            ('2', NumBase::Octal | NumBase::Decimal | NumBase::Hexadecimal) => Some(2),
            ('3', NumBase::Octal | NumBase::Decimal | NumBase::Hexadecimal) => Some(3),
            ('4', NumBase::Octal | NumBase::Decimal | NumBase::Hexadecimal) => Some(4),
            ('5', NumBase::Octal | NumBase::Decimal | NumBase::Hexadecimal) => Some(5),
            ('6', NumBase::Octal | NumBase::Decimal | NumBase::Hexadecimal) => Some(6),
            ('7', NumBase::Octal | NumBase::Decimal | NumBase::Hexadecimal) => Some(7),
            (c, NumBase::Octal) => {
                return Err(format!("Got invalid char {c} in octal string.").into())
            }
            ('8', NumBase::Decimal | NumBase::Hexadecimal) => Some(8),
            ('9', NumBase::Decimal | NumBase::Hexadecimal) => Some(9),
            (c, NumBase::Decimal) => {
                return Err(format!("Got invalid char {c} in decimal string.").into())
            }
            ('a' | 'A', NumBase::Hexadecimal) => Some(10),
            ('b' | 'B', NumBase::Hexadecimal) => Some(11),
            ('c' | 'C', NumBase::Hexadecimal) => Some(12),
            ('d' | 'D', NumBase::Hexadecimal) => Some(13),
            ('e' | 'E', NumBase::Hexadecimal) => Some(14),
            ('f' | 'F', NumBase::Hexadecimal) => Some(15),
            (c, NumBase::Hexadecimal) => {
                return Err(format!("Got invalid char {c} in hex string.").into())
            }
            ('=', NumBase::Base64) => None,
            ('A', NumBase::Base64) => Some(0),
            ('B', NumBase::Base64) => Some(1),
            ('C', NumBase::Base64) => Some(2),
            ('D', NumBase::Base64) => Some(3),
            ('E', NumBase::Base64) => Some(4),
            ('F', NumBase::Base64) => Some(5),
            ('G', NumBase::Base64) => Some(6),
            ('H', NumBase::Base64) => Some(7),
            ('I', NumBase::Base64) => Some(8),
            ('J', NumBase::Base64) => Some(9),
            ('K', NumBase::Base64) => Some(10),
            ('L', NumBase::Base64) => Some(11),
            ('M', NumBase::Base64) => Some(12),
            ('N', NumBase::Base64) => Some(13),
            ('O', NumBase::Base64) => Some(14),
            ('P', NumBase::Base64) => Some(15),
            ('Q', NumBase::Base64) => Some(16),
            ('R', NumBase::Base64) => Some(17),
            ('S', NumBase::Base64) => Some(18),
            ('T', NumBase::Base64) => Some(19),
            ('U', NumBase::Base64) => Some(20),
            ('V', NumBase::Base64) => Some(21),
            ('W', NumBase::Base64) => Some(22),
            ('X', NumBase::Base64) => Some(23),
            ('Y', NumBase::Base64) => Some(24),
            ('Z', NumBase::Base64) => Some(25),
            ('a', NumBase::Base64) => Some(26),
            ('b', NumBase::Base64) => Some(27),
            ('c', NumBase::Base64) => Some(28),
            ('d', NumBase::Base64) => Some(29),
            ('e', NumBase::Base64) => Some(30),
            ('f', NumBase::Base64) => Some(31),
            ('g', NumBase::Base64) => Some(32),
            ('h', NumBase::Base64) => Some(33),
            ('i', NumBase::Base64) => Some(34),
            ('j', NumBase::Base64) => Some(35),
            ('k', NumBase::Base64) => Some(36),
            ('l', NumBase::Base64) => Some(37),
            ('m', NumBase::Base64) => Some(38),
            ('n', NumBase::Base64) => Some(39),
            ('o', NumBase::Base64) => Some(40),
            ('p', NumBase::Base64) => Some(41),
            ('q', NumBase::Base64) => Some(42),
            ('r', NumBase::Base64) => Some(43),
            ('s', NumBase::Base64) => Some(44),
            ('t', NumBase::Base64) => Some(45),
            ('u', NumBase::Base64) => Some(46),
            ('v', NumBase::Base64) => Some(47),
            ('w', NumBase::Base64) => Some(48),
            ('x', NumBase::Base64) => Some(49),
            ('y', NumBase::Base64) => Some(50),
            ('z', NumBase::Base64) => Some(51),
            ('0', NumBase::Base64) => Some(52),
            ('1', NumBase::Base64) => Some(53),
            ('2', NumBase::Base64) => Some(54),
            ('3', NumBase::Base64) => Some(55),
            ('4', NumBase::Base64) => Some(56),
            ('5', NumBase::Base64) => Some(57),
            ('6', NumBase::Base64) => Some(58),
            ('7', NumBase::Base64) => Some(59),
            ('8', NumBase::Base64) => Some(60),
            ('9', NumBase::Base64) => Some(61),
            ('+', NumBase::Base64) => Some(62),
            ('/', NumBase::Base64) => Some(63),
            (ch, base) => return Err(format!("Got unknown char {ch} in base {base}").into()),
        };

        Ok(val)
    }

    /// Iterate over the digits of the number, converting them into their
    /// actual value, returning (whole, frac) sequences in the order the
    /// characters were given.
    pub fn digit_values(&self) -> Result<(Vec<u32>, Vec<u32>)> {
        let Self { base, digits, .. } = self;
        let base = base.unwrap_or_default();
        match digits {
            NumSeq::WholeOnly { text, .. } => {
                let (vals, errs): (Vec<_>, Vec<_>) = text
                    .chars()
                    .map(|c| Self::char_to_val(c, base))
                    .partition_result();
                if errs.len() > 0 {
                    Err(error_tree("Got invalid chars in {self}", errs))
                } else {
                    Ok((
                        vals.into_iter().filter_map(|i| i).collect(),
                        Vec::with_capacity(0),
                    ))
                }
            }
            NumSeq::BothParts { whole, frac } => {
                let (whole_vals, mut whole_errs): (Vec<_>, Vec<_>) = whole
                    .chars()
                    .map(|c| Self::char_to_val(c, base))
                    .partition_result();

                let (frac_vals, frac_errs): (Vec<_>, Vec<_>) = frac
                    .chars()
                    .map(|c| Self::char_to_val(c, base))
                    .partition_result();

                whole_errs.extend(frac_errs.into_iter());

                if whole_errs.len() > 0 {
                    whole_errs.insert(0, self.treeify());
                    Err(error_tree(
                        format!("got invalid chars in number"),
                        whole_errs,
                    ))
                } else {
                    Ok((
                        whole_vals.into_iter().filter_map(|i| i).collect(),
                        frac_vals.into_iter().filter_map(|i| i).collect(),
                    ))
                }
            }
            NumSeq::FracOnly(frac) => {
                let (vals, errs): (Vec<_>, Vec<_>) = frac
                    .chars()
                    .map(|c| Self::char_to_val(c, base))
                    .partition_result();
                if errs.len() > 0 {
                    Err(error_tree("Got invalid chars in {self}", errs))
                } else {
                    Ok((
                        Vec::with_capacity(0),
                        vals.into_iter().filter_map(|i| i).collect(),
                    ))
                }
            }
        }
    }
}

impl ConcreteSyntax for Number {
    const EXPECTED_RULE: Rule = Rule::num_lit;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let mut pairs = pair.into_inner();

        // We expect either one or two pairs from this rule. If the first is the
        // sign, the second will be the number.
        let (digits, sign) = {
            let first = pairs.next().unwrap();
            let second = pairs.next();

            if let Some(second) = second {
                let sign = Sign::parse_pair(first)?;
                (second, Some(sign))
            } else {
                (first, None)
            }
        };

        // Take the parsed numbers, and break down
        let num_rule = digits.as_rule();
        let inner = digits.into_inner();
        let number = match num_rule {
            // Binary
            Rule::binary_rational => Number {
                base: Some(NumBase::Binary),
                sign,
                digits: NumSeq::parse_rational(true, inner)?,
            },
            Rule::binary_fraction => Number {
                base: Some(NumBase::Binary),
                sign,
                digits: NumSeq::parse_frac(true, inner)?,
            },
            Rule::binary_whole => Number {
                base: Some(NumBase::Binary),
                sign,
                digits: NumSeq::parse_whole(true, inner)?,
            },
            // Octal
            Rule::octal_rational => Number {
                base: Some(NumBase::Octal),
                sign,
                digits: NumSeq::parse_rational(true, inner)?,
            },
            Rule::octal_fraction => Number {
                base: Some(NumBase::Octal),
                sign,
                digits: NumSeq::parse_frac(true, inner)?,
            },
            Rule::octal_whole => Number {
                base: Some(NumBase::Octal),
                sign,
                digits: NumSeq::parse_whole(true, inner)?,
            },
            // Hexadecimal
            Rule::hexadecimal_rational => Number {
                base: Some(NumBase::Hexadecimal),
                sign,
                digits: NumSeq::parse_rational(true, inner)?,
            },
            Rule::hexadecimal_fraction => Number {
                base: Some(NumBase::Hexadecimal),
                sign,
                digits: NumSeq::parse_frac(true, inner)?,
            },
            Rule::hexadecimal_whole => Number {
                base: Some(NumBase::Hexadecimal),
                sign,
                digits: NumSeq::parse_whole(true, inner)?,
            },
            // Base 64
            Rule::base64_rational => Number {
                base: Some(NumBase::Base64),
                sign,
                digits: NumSeq::parse_rational(true, inner)?,
            },
            Rule::base64_fraction => Number {
                base: Some(NumBase::Base64),
                sign,
                digits: NumSeq::parse_frac(true, inner)?,
            },
            Rule::base64_whole => Number {
                base: Some(NumBase::Base64),
                sign,
                digits: NumSeq::parse_whole(true, inner)?,
            },
            // Decimal
            Rule::decimal_rational => {
                let base = if inner
                    .peek()
                    .filter(|p| p.as_rule() == Rule::DEC_SIGIL)
                    .is_some()
                {
                    Some(NumBase::Decimal)
                } else {
                    None
                };

                Number {
                    base,
                    sign,
                    digits: NumSeq::parse_rational(base.is_some(), inner)?,
                }
            }
            Rule::decimal_fraction => {
                let base = if inner
                    .peek()
                    .filter(|p| p.as_rule() == Rule::DEC_SIGIL)
                    .is_some()
                {
                    Some(NumBase::Decimal)
                } else {
                    None
                };

                Number {
                    base,
                    sign,
                    digits: NumSeq::parse_frac(base.is_some(), inner)?,
                }
            }
            Rule::decimal_whole => {
                let base = if inner
                    .peek()
                    .filter(|p| p.as_rule() == Rule::DEC_SIGIL)
                    .is_some()
                {
                    Some(NumBase::Decimal)
                } else {
                    None
                };

                Number {
                    base,
                    sign,
                    digits: NumSeq::parse_whole(base.is_some(), inner)?,
                }
            }
            rule => Err(Tree::Leaf(format!(
                "Should be unreachable. Illegal rule was {:?}",
                rule
            )))?,
        };
        Ok(number)
    }
}

impl TreeFormat for Number {
    fn treeify(&self) -> Tree<String> {
        let head = "Number".to_owned();

        let Number { base, sign, digits } = self;
        let mut sub_trees = Vec::with_capacity(3);
        if let Some(base) = base {
            sub_trees.push(base.to_string().into())
        }
        if let Some(sign) = sign {
            sub_trees.push(sign.to_string().into())
        }
        sub_trees.push(digits.to_string().into());
        sub_trees.shrink_to_fit();

        Tree::Branch(head, sub_trees)
    }
}

#[cfg(test)]
mod test {
    use pest::Parser;

    use crate::concrete_syntax::ZamParser;

    use super::*;

    fn parse_number(input: &str) -> Number {
        let pair = ZamParser::parse(Rule::num_lit, input)
            .expect("Failed to parse test string")
            .next()
            .expect("Successful parse yielded no pairs");
        Number::parse_pair(pair).expect("Failed to parse successfully parsed pair")
    }

    #[test]
    fn decimal_int() {
        let test_str = "15";
        let want = Number {
            base: None,
            sign: None,
            digits: NumSeq::WholeOnly {
                has_dot: false,
                text: "15".to_string(),
            },
        };

        let got = parse_number(test_str);
        assert_eq!(want, got);
    }

    #[test]
    fn binary_frac() {
        let test_str = "0b.1001101";
        let want = Number {
            base: Some(NumBase::Binary),
            sign: None,
            digits: NumSeq::FracOnly("1001101".to_string()),
        };

        let got = parse_number(test_str);
        assert_eq!(got, want);
    }

    #[test]
    fn complex() {
        let test_str = "-0x45ad.6e2";
        let want = Number {
            base: Some(NumBase::Hexadecimal),
            sign: Some(Sign::Negative),
            digits: NumSeq::BothParts {
                whole: "45ad".to_string(),
                frac: "6e2".to_string(),
            },
        };
        let got = parse_number(test_str);
        assert_eq!(got, want);
    }
}
