use crate::{
    concrete_syntax::{
        branches::{if_branch::If, match_expr::Match},
        comments::Comment,
        error_tree, fmt_pair,
        lambda::Lambda,
        literals::Literal,
        sequences::{Block, List, Tuple},
        symbols::Symbol,
        take_exactly, ConcreteSyntax, Rule,
    },
    result::Result,
};

use itertools::Itertools;
use pest::iterators::Pair;
use rosary::{format::TreeFormat, vec_tree::Tree};

#[derive(Debug, Clone, PartialEq)]
pub enum InnerExpr {
    If(Box<If>),
    Match(Box<Match>),
    Block(Box<Block>),
    Lit(Literal),
    Tuple(Tuple),
    List(List),
    // Repetition without parens
    Juxtaposed(Vec<Expr>),
    Lambda(Box<Lambda>),
}

impl ConcreteSyntax for InnerExpr {
    const EXPECTED_RULE: Rule = Rule::inner_expr;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let (exprs, errs): (Vec<_>, Vec<_>) = pair
            .into_inner()
            .map(|pair| match pair.as_rule() {
                Rule::match_expr => Match::parse_pair(pair).map(|expr| Self::Match(Box::new(expr))),
                Rule::block => Block::parse_pair(pair).map(|expr| Self::Block(Box::new(expr))),
                Rule::literal => Literal::parse_pair(pair).map(Self::Lit),
                Rule::tuple => Tuple::parse_pair(pair).map(|t| t.collapse()),
                Rule::list => List::parse_pair(pair).map(Self::List),
                Rule::if_expr => If::parse_pair(pair).map(|expr| Self::If(Box::new(expr))),
                Rule::lambda_expr => {
                    Lambda::parse_pair(pair).map(|lambda| Self::Lambda(Box::new(lambda)))
                }
                rule => Err(error_tree(
                    format!("Unexpected rule {:?}", rule),
                    vec![fmt_pair(&pair)],
                )),
            })
            .partition_result();

        if errs.len() > 0 {
            return Err(error_tree(
                "Got errors parsing inner expression sequence",
                errs,
            ));
        }

        match exprs.len() {
            0 => Err(format!(
                "{:?} should always have at least one inner rule",
                Self::EXPECTED_RULE
            )
            .into()),
            1 => Ok(exprs
                .into_iter()
                .next()
                .expect("Should not fail to pop element after checking length")),
            _ => Ok(Self::Juxtaposed(
                exprs.into_iter().map(Into::into).collect(),
            )),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Expr {
    pre_comment: Option<Comment>,
    post_comment: Option<Comment>,
    inner_expr: InnerExpr,
}

impl Expr {
    pub fn to_inner(self) -> InnerExpr {
        self.inner_expr
    }
}

impl ConcreteSyntax for Expr {
    const EXPECTED_RULE: Rule = Rule::expr;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let [pair] = take_exactly(pair)?;

        match pair.as_rule() {
            Rule::pre_comment_expr => {
                let [pre_comment, expr] = take_exactly(pair)?;
                let pre_comment = Comment::parse_pair(pre_comment)?;
                let inner_expr = InnerExpr::parse_pair(expr)?;
                Ok(Expr {
                    pre_comment: Some(pre_comment),
                    post_comment: None,
                    inner_expr,
                })
            }
            Rule::inner_expr => {
                let inner_expr = InnerExpr::parse_pair(pair)?;
                Ok(Expr {
                    pre_comment: None,
                    post_comment: None,
                    inner_expr,
                })
            }
            Rule::post_comment_expr => {
                let [expr, post] = take_exactly(pair)?;
                let inner_expr = InnerExpr::parse_pair(expr)?;
                let post_comment = Comment::parse_pair(post)?;
                Ok(Expr {
                    pre_comment: None,
                    post_comment: Some(post_comment),
                    inner_expr,
                })
            }
            Rule::both_comment_expr => {
                let [pre, expr, post] = take_exactly(pair)?;
                let pre_comment = Comment::parse_pair(pre)?;
                let inner_expr = InnerExpr::parse_pair(expr)?;
                let post_comment = Comment::parse_pair(post)?;
                Ok(Expr {
                    pre_comment: Some(pre_comment),
                    post_comment: Some(post_comment),
                    inner_expr,
                })
            }
            rule => Err(format!("Unexpected inner rule {:?} inside of expression.", rule).into()),
        }
    }
}

impl<I: Into<InnerExpr>> From<I> for Expr {
    fn from(inner_expr: I) -> Self {
        Self {
            pre_comment: None,
            post_comment: None,
            inner_expr: inner_expr.into(),
        }
    }
}
impl From<If> for InnerExpr {
    fn from(iff: If) -> Self {
        InnerExpr::If(Box::new(iff))
    }
}
impl From<Literal> for InnerExpr {
    fn from(lit: Literal) -> Self {
        InnerExpr::Lit(lit)
    }
}
impl From<Tuple> for InnerExpr {
    fn from(sexpr: Tuple) -> Self {
        InnerExpr::Tuple(sexpr)
    }
}
impl From<Match> for InnerExpr {
    fn from(mtch: Match) -> Self {
        InnerExpr::Match(Box::new(mtch))
    }
}
impl From<Block> for InnerExpr {
    fn from(block: Block) -> Self {
        InnerExpr::Block(Box::new(block))
    }
}
impl From<Symbol> for InnerExpr {
    fn from(sym: Symbol) -> Self {
        InnerExpr::Lit(Literal::Sym(sym.into()))
    }
}
impl From<List> for InnerExpr {
    fn from(list: List) -> Self {
        InnerExpr::List(list)
    }
}

/// Formatting trait
impl TreeFormat for InnerExpr {
    fn treeify(&self) -> Tree<String> {
        match self {
            Self::If(if_expr) => if_expr.treeify(),
            Self::Match(match_expr) => match_expr.treeify(),
            Self::Block(block) => block.treeify(),
            Self::Lit(lit) => lit.treeify(),
            Self::Tuple(tuple) => tuple.treeify(),
            Self::List(list) => list.treeify(),
            Self::Juxtaposed(exprs) => Tree::Branch(
                "Juxtaposed".to_string(),
                exprs.iter().map(TreeFormat::treeify).collect(),
            ),
            Self::Lambda(lambda) => lambda.treeify(),
        }
    }
}

impl TreeFormat for Expr {
    fn treeify(&self) -> Tree<String> {
        let mut children = Vec::with_capacity(3);

        if let Some(ref pre_comment) = self.pre_comment {
            children.push(Tree::Branch(
                format!("Head Comment"),
                vec![pre_comment.treeify()],
            ))
        }
        children.push(self.inner_expr.treeify());
        if let Some(ref post_comment) = self.post_comment {
            children.push(Tree::Branch(
                format!("Tail Comment"),
                vec![post_comment.treeify()],
            ));
        }

        Tree::Branch(format!("Commented Expression"), children)
    }
}

#[cfg(test)]
mod expr_tests {
    use crate::concrete_syntax::pattern::Pattern;

    use super::*;

    #[test]
    fn bare_expr() {
        let got = Expr::test_parse("45 + val");
        let want = InnerExpr::Juxtaposed(vec![
            Literal::test_parse("45").into(),
            Symbol::test_parse("+").into(),
            Symbol::test_parse("val").into(),
        ])
        .into();

        assert_eq!(got, want);
    }

    #[test]
    fn bounded_expr() {
        let got = Expr::test_parse("(45 + val)");
        let want = InnerExpr::Tuple(
            vec![Expr::from(InnerExpr::Juxtaposed(vec![
                InnerExpr::from(Literal::test_parse("45")).into(),
                InnerExpr::from(Symbol::test_parse("+")).into(),
                InnerExpr::from(Symbol::test_parse("val")).into(),
            ]))]
            .into(),
        )
        .into();

        assert_eq!(
            got,
            want,
            "Actual parse: {}\nExpected parse: {}",
            got.treeify(),
            want.treeify()
        );
    }

    #[test]
    fn lamba_expr() {
        let got = Expr::test_parse("fn foo => foo");
        let want = InnerExpr::Lambda(Box::new(Lambda {
            args: vec![Pattern::test_parse("foo")],
            body: Expr::test_parse("foo"),
        }))
        .into();

        assert_eq!(got, want)
    }
}
