use pest::iterators::Pair;
use rosary::{format::TreeFormat, vec_tree::Tree};

use crate::{
    concrete_syntax::{drop_next, error, expr::Expr, pattern::Pattern, ConcreteSyntax, Rule},
    result::Result,
};

#[derive(Debug, Clone, PartialEq)]
pub struct BindingPattern {
    pub pattern: Pattern,
    pub type_annotation: Option<Expr>,
    pub value: Expr,
}

impl ConcreteSyntax for BindingPattern {
    const EXPECTED_RULE: Rule = Rule::binding_pattern;

    fn parse_impl(pair: Pair<Rule>) -> Result<Self> {
        let mut pairs = pair.into_inner();

        let pattern = Pattern::parse_next("Missing Pattern", &mut pairs)?;

        let token = pairs
            .next()
            .ok_or_else(|| error("Failed to parse either : or = in binding pattern?"))?;

        let type_annotation = match token.as_rule() {
            Rule::COLON => {
                let type_annotation =
                    Some(Expr::parse_next("Missing Type Expression", &mut pairs)?);
                drop_next(&mut pairs, Rule::EQUAL)?;
                type_annotation
            }
            Rule::EQUAL => None,
            rule => return Err(format!("Got unexpected Rule::{:?} instead of : or =", rule).into()),
        };

        let value = Expr::parse_next("Missing value in binding pattern", &mut pairs)?;

        Ok(BindingPattern {
            pattern,
            type_annotation,
            value,
        })
    }
}

impl TreeFormat for BindingPattern {
    fn treeify(&self) -> Tree<String> {
        Tree::Branch(
            "Binding".to_owned(),
            vec![
                Some(self.pattern.treeify()),
                self.type_annotation
                    .as_ref()
                    .map(|t| t.treeify().swap_head("Type Annotation".to_string()).1),
                Some(self.value.treeify().swap_head("Value".to_string()).1),
            ]
            .into_iter()
            .filter_map(|c| c)
            .collect(),
        )
    }
}
